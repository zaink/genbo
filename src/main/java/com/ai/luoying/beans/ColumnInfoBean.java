package com.ai.luoying.beans;

import lombok.Data;

/**
 * @author zenghy
 * @date 2019/3/12
 **/
@Data
public class ColumnInfoBean {

    private boolean keyIs;

    private int keySeq;

    private String tableName;

    private String columnName;

    private int columnType;

    private String columnTypeName;

    private String columnRemark;

    private int colunmPrecision;

    private int colunmScale;

    private String firstUpperName;

    private String firstLowerName;

    private String varName;

    private String boDataType;

    private String boDataType2;

    private Boolean idxIs;
}
