package com.ai.luoying.beans;

import com.ai.luoying.genbofile.ColumnInfo;
import lombok.Data;

import java.util.List;

/**
 * @author zenghy
 * @date 2019/3/11
 **/
@Data
public class DataBean {

    private List<ColumnInfoBean> columnInfos;
    private Boolean routeFlag;
    // bo
    private String boName;
    private String boPath;
    private String boFile;
    private String boFilePath;
    private String boPackage;
    // bean
    private String beanClass;
    private String beanFile;
    private String beanPath;
    private String beanFilePath;
    private String beanPackage;
    // value
    private String valueClass;
    private String valueFile;
    private String valuePath;
    private String valueFilePath;
    private String valuePackage;
    // engine
    private String engineClass;
    private String engineFile;
    private String enginePath;
    private String engineFilePath;
    private String enginePakage;
    //DAO
    private String daoClass;
    private String daoFile;
    private String daoPath;
    private String daoFilePath;
    private String daoPackage;
    //DAOImpl
    private String daoImplClass;
    private String daoImplFile;
    private String daoImplPath;
    private String daoImplFilePath;
    private String daoImplPackage;
}
