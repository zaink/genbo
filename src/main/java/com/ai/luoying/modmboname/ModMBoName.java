package com.ai.luoying.modmboname;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import com.ai.luoying.util.FileInfo;
import com.ai.luoying.util.IniReader;
import com.ai.luoying.util.SysLog;
import com.ai.luoying.util.VerUtil;

public class ModMBoName
{
    public static void crtLogFile(MBoCtrlInfo ctrl) throws Exception
    {
        ctrl.logName = SysLog.getLogFile(ctrl.iniName);

        SysLog.log(ctrl.logName, VerUtil.ver);

        SysLog.log(ctrl.logName, "配置文件[" + ctrl.iniName + "]");
    }

    public static void getIniFile(String[] args, MBoCtrlInfo ctrl, String defFile) throws Exception
    {
        if (args.length == 0)
        {
            File directory = new File("");

            String nowDir = directory.getAbsolutePath();

            ctrl.iniName = nowDir + defFile;
        }
        else
        {
            ctrl.iniName = args[0];
        }

        File iniFile = new File(ctrl.iniName);

        if (!iniFile.isFile() || !iniFile.exists())
        {
            System.out.println("异常退出，配置文件不存在[" + ctrl.iniName + "]");

            throw new Exception("ini file not found");
        }
    }

    public static void modName(MBoCtrlInfo ctrl, FileInfo file) throws Exception
    {
        BufferedReader reader = null;

        try
        {
            reader = new BufferedReader(new FileReader(file.filePathName));
            String fileLine = null;
            String m_boName1 = "";
            String m_boName2 = "";

            int indexLine = 1;
            int pos = 0;

            Boolean find = false;

            List<String> list = new ArrayList<String>();

            // 一次读入一行，直到读入null为文件结束
            while ((fileLine = reader.readLine()) != null)
            {
                if (fileLine.matches(".*private.*String.*m_boName.*"))
                {
                    find = true;

                    pos = fileLine.indexOf("=");

                    m_boName1 = fileLine.substring(pos + 1).trim();
                    m_boName1 = m_boName1.replace("\"", "");
                    m_boName1 = m_boName1.replace(";", "");

                    m_boName2 = file.filePackage + "" + file.fileNameNoSuffix;

                    if (!m_boName1.equals(m_boName2))
                    {
                        SysLog.log(ctrl.logName, file.filePathName + "[" + indexLine + "][" + m_boName1 + "]->[" + m_boName2 + "]");
                    }

                    // 替换
                    fileLine = fileLine.replace(m_boName1, m_boName2);
                }

                list.add(fileLine);

                indexLine++;
            }

            reader.close();

            if (find && ctrl.isMod)
            {
                PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file.filePathName)));

                for (int i = 0; i < list.size(); i++)
                {
                    out.println(list.get(i));
                }

                out.close();
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (reader != null)
            {
                try
                {
                    reader.close();
                }
                catch (IOException e1)
                {
                }
            }
        }
    }

    public static void readIniFile(MBoCtrlInfo ctrl) throws Exception
    {
        // 读配置信息
        IniReader reader = new IniReader(ctrl.iniName);

        // 文件列表
        ctrl.fileList = reader.getValueList("pathlist");

        // 是否执行更改
        ctrl.isMod = Boolean.valueOf(reader.getValue("ctrl", "ismod", "true"));
    }
}
