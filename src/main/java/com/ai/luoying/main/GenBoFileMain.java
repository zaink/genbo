package com.ai.luoying.main;

import java.util.HashMap;
import java.util.Iterator;

import com.ai.luoying.genbofile.BoCtrlInfo;
import com.ai.luoying.genbofile.GenBoFile;
import com.ai.luoying.genbofile.XlsCtrlInfo;
import com.ai.luoying.util.StrUtil;
import com.ai.luoying.util.SysLog;

public class GenBoFileMain
{
    public static void main(String[] args) throws Exception
    {
        // 控制信息
        BoCtrlInfo ctrl = new BoCtrlInfo();

        // 得到配置文件
        GenBoFile.getIniFile(args, ctrl, "D:\\touchui\\genbo\\src\\main\\resources\\ini\\GenBoFile.ini");

        // 创建日志文件
        GenBoFile.crtLogFile(ctrl);

        // 读取配置文件
        GenBoFile.readIniFile(ctrl);

        // 得到配置文件
        GenBoFile.getXlsFile(args, ctrl, "D:\\touchui\\genbo\\src\\main\\resources\\ini\\GenBoFile.xlsx");

        // 读取配置文件
        GenBoFile.readXlsFile(ctrl);

        for (String dbname : ctrl.dbList)
        {
            ctrl.dbname = dbname;
            System.out.println("数据库名字---------------" + ctrl.dbname);
            System.out.println("数据库名字---------------" + ctrl.xlsMap.toString());

            HashMap<String, XlsCtrlInfo> xlsctrl = ctrl.xlsMap.get(dbname);

            Iterator<String> it = xlsctrl.keySet().iterator();

            while (it.hasNext())
            {
                String key = it.next();

                ctrl.xlsctrlinfo = xlsctrl.get(key);

                String hint = dbname + "" + ctrl.xlsctrlinfo.tableName + " [" + ctrl.xlsctrlinfo.center + "] [" + ctrl.xlsctrlinfo.funmodel;

                SysLog.log(ctrl.logName, "");
                SysLog.log(ctrl.logName, "生成table对象===>" + hint + "]开始");

                ctrl.stdName = StrUtil.firstUpperName(ctrl.xlsctrlinfo.tableName);

                ctrl.boName = "BO" + ctrl.stdName;

                GenBoFile.genAll(ctrl);

                SysLog.log(ctrl.logName, "生成table对象===>" + hint + "]结束");
            }
        }

        // close conn
        GenBoFile.destory(ctrl);

        /*
        for (String dbname : ctrl.dbList)
        {
            ctrl.dbname = dbname;

            HashMap<String, XlsCtrlInfo> xlsctrl = ctrl.xlsMap.get(dbname);

            Iterator<String> it = xlsctrl.keySet().iterator();

            while (it.hasNext())
            {
                String key = it.next();

                ctrl.xlsctrlinfo = xlsctrl.get(key);

                String hint = dbname + "." + ctrl.xlsctrlinfo.tableName + " [" + ctrl.xlsctrlinfo.center + "] [" + ctrl.xlsctrlinfo.funmodel;

                //
                SysLog.log(ctrl.logName, "");
                SysLog.log(ctrl.logName, "生成dao/sv文件===>" + hint + "]");

                ctrl.stdName = StrUtil.firstUpperName(ctrl.xlsctrlinfo.tableName);

                ctrl.boName = "BO" + ctrl.stdName;

                GenBoFile.mkdirDaoSvInfo(ctrl);
            }
        }
        */

        // gen dao sv info
//        GenBoFile11.genDaoSvCtrlInfo(ctrl);

        //
        SysLog.log(ctrl.logName, "");
        SysLog.log(ctrl.logName, "GenBoFile11Main[处理结束]");
    }
}
