package com.ai.luoying.genbofile;

import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.List;

public class BoCtrlInfo
{
    public XlsCtrlInfo xlsctrlinfo;

    public String iniName;

    public String xlsName;

    public String logName;

    public boolean isShow;

    public LinkedHashMap<String, DBCtrlInfo> dbMap = new LinkedHashMap<String, DBCtrlInfo>();

    public LinkedHashMap<String, LinkedHashMap<String, XlsCtrlInfo>> xlsMap = new LinkedHashMap<String, LinkedHashMap<String, XlsCtrlInfo>>();

    public LinkedHashMap<String, PrintWriter> daosvFwMap = new LinkedHashMap<String, PrintWriter>();

    public LinkedHashMap<String, List<String>> daosvSrcMap = new LinkedHashMap<String, List<String>>();

    public List<String> dbList;

    public List<String> nvar2List;

    public String dbname;

    public String modelHomePath;

    // bo
    public String boPath;

    public String boFile;

    public String boFilePath;

    public String boPackage;

    // bo
    public String boPath51;

    public String boFile51;

    public String boFilePath51;

    public String boPackage51;

    public int keyCnt;

    public List<ColumnInfo> keyList;

    public String stdName;

    public String boName;

    public String m_boName;

    // bean
    public String beanClass;

    public String beanFile;

    public String beanPath;

    public String beanFilePath;

    public String beanPackage;

    // engine
    public String engineClass;

    public String engineFile;

    public String enginePath;

    public String engineFilePath;

    public String enginePakage;

    // value
    public String valueClass;

    public String valueFile;

    public String valuePath;

    public String valueFilePath;

    public String valuePackage;

    // 参数
    public String paramStr = "";

    public String paramValue = "";

    //
    public String svImplClass;

    public String svImplFile;

    public String svImplPath;

    public String svImplPackage;

    //
    public String svItfClass;

    public String svItfFile;

    public String svItfPath;

    //
    public String svcClass;

    public String svcFile;

    public String svcPath;

    public String svcFilePath;

    public String svcPackage;

    public String keyStr;

    //DAO
    public String daoClass;
    public String daoFile;
    public String daoPath;
    public String daoFilePath;
    public String daoPackage;

    //DAOImpl
    public String daoImplClass;
    public String daoImplFile;
    public String daoImplPath;
    public String daoImplFilePath;
    public String daoImplPackage;
}
