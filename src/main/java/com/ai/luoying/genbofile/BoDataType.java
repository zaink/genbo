package com.ai.luoying.genbofile;

public class BoDataType
{
    public static final String CharType = "Char";

    public static final String StringType = "String";

    public static final String DateTimeType = "DateTime";

    public static final String DateType = "Date";

    public static final String DoubleType = "Double";

    public static final String IntegerType = "Integer";

    public static final String longType = "long";

    public static final String intType = "int";

    public static final String LongType = "Long";
}
