package com.ai.luoying.genbofile;

import com.ai.luoying.beans.DataBean;
import com.ai.luoying.util.StrUtil;
import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Map;

/**
 * @author zenghy
 * @date 2019/3/11
 **/
@Slf4j
public class FreemarkerBuilder {

    public static final Configuration cfg = new Configuration(Configuration.VERSION_2_3_28);

    public static void build(DataBean data, String tpltName, String outPath) {
        Template temp = null;
        FileOutputStream fos = null;
        try {
            temp = cfg.getTemplate(tpltName);
            fos = new FileOutputStream(outPath);
            temp.process(data, new OutputStreamWriter(fos, StrUtil.encoding));
        } catch (Exception e) {
            try {
                if (null != fos) {
                    fos.flush();
                    fos.close();
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }
    }

    public static void build(Map map,String tpltName,String outPath){
        Template temp = null;
        FileOutputStream fos = null;
        try {
            temp = cfg.getTemplate(tpltName);
            fos = new FileOutputStream(outPath);
            temp.process(map, new OutputStreamWriter(fos, StrUtil.encoding));
        } catch (Exception e) {
            try {
                if (null != fos) {
                    fos.flush();
                    fos.close();
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }
    }


    static  {
        try {
            cfg.setDirectoryForTemplateLoading(new File("src/main/resources/freemarker"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        cfg.setDefaultEncoding("UTF-8");
        cfg.setLogTemplateExceptions(false);
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
    }
}
