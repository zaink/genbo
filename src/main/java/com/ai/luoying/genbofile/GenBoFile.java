package com.ai.luoying.genbofile;

import com.ai.luoying.beans.ColumnInfoBean;
import com.ai.luoying.beans.DataBean;
import com.ai.luoying.util.*;
import com.ai.luoying.util.zl.OracleSQLSyntaxAnalyzer;
import com.ai.luoying.util.zl.interfaces.ISQLSyntaxAnalyzer;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.net.URLDecoder;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public class GenBoFile {
    private static void chgColumnInfo(BoCtrlInfo ctrl, List<ColumnInfo> columnList) {
        for (ColumnInfo ci : columnList) {
            // 首字母大写名字
            ci.firstUpperName = StrUtil.firstUpperName(ci.columnName);
            ci.firstLowerName = StrUtil.firstLowerName(ci.columnName);
            // 变量名字
            ci.varName = "S_" + ci.firstUpperName;

            // 转字段类型
            if (ci.columnTypeName.equals("VARCHAR2")) {
                ci.boDataType = BoDataType.StringType;
            } else if (ci.columnTypeName.equals("DATE")) {
                ci.boDataType = BoDataType.DateTimeType;
            } else if (ci.columnTypeName.equals("CHAR")) {
                ci.boDataType = BoDataType.StringType;
            } else if (ci.columnTypeName.equals("NUMBER")) {
                if (ci.colunmScale > 0) {
                    ci.boDataType = BoDataType.DoubleType;
                } else if (ci.colunmPrecision <= 6) {
                    ci.boDataType = BoDataType.IntegerType;
                } else {
                    ci.boDataType = BoDataType.LongType;
                }
            }
        }
    }

    private static void closeDaoSvCtrlInf(BoCtrlInfo ctrl) throws Exception {
        PrintWriter out = null;
        String filepath;

        Iterator<String> ait;
        ait = ctrl.daosvFwMap.keySet().iterator();

        while (ait.hasNext()) {
            filepath = ait.next();

            out = ctrl.daosvFwMap.get(filepath);

            out.close();
        }
    }

    public static void crtLogFile(BoCtrlInfo ctrl) throws Exception {
        ctrl.logName = SysLog.getLogFile(ctrl.iniName);

        SysLog.log(ctrl.logName, VerUtil.ver);

        SysLog.log(ctrl.logName, "日志文件[" + ctrl.logName + "]");
    }

    public static void destory(BoCtrlInfo ctrl) throws Exception {
        SysLog.log(ctrl.logName, "");

        Connection con;
        DBCtrlInfo db;

        Iterator<String> it = ctrl.dbMap.keySet().iterator();

        while (it.hasNext()) {
            String key = it.next();

            db = ctrl.dbMap.get(key);

            if (db.con != null) {
                db.con.close();

                SysLog.log(ctrl.logName, "数据库连接已关闭[" + db.dbname + "]");
            }
        }
    }

    public static void genAll(BoCtrlInfo ctrl) throws Exception {
        List<ColumnInfo> columnList = new ArrayList<>();


        // 得到bo控制信息
        DataBean data = setBoCtrlInfo(ctrl);

        // 从数据库得到字段信息
        getColumnInfo(ctrl, columnList);

        // 转换字段转义
        chgColumnInfo(ctrl, columnList);

        // 显示字段信息
        showColumnInfo(ctrl, columnList);

        // 生成 bo 配置文件
        genBo(ctrl, columnList);

        // 生成 bean java
        genBean(ctrl, columnList);

        // 生成engin java
        genEngine2(ctrl, columnList);

        // 生成ivalue java
        genValue(ctrl, columnList);

        // 得到dao sv控制信息
        setDaoSvCtrlInfo(ctrl);

        // 得到dao文件信息
        setDaoFileInfo(ctrl);

        // 得到sv文件信息
        setSvFileInfo(ctrl);

        buildColumnList(columnList, data);
        // 生成IDAO文件
        genIDao(ctrl, data);
        //生成IDAOIMPL
        genIDaoImpl(ctrl, data);
        //生成SQL文件
        genSqlFile(ctrl, data);
    }

    private static void genSqlFile(BoCtrlInfo ctrl, DataBean data) {

        List<ColumnInfoBean> columnInfos = data.getColumnInfos();
        List<String> colNameList = columnInfos.stream().map(ColumnInfoBean::getColumnName).collect(Collectors.toList());

        columnInfos.stream().filter(ColumnInfoBean::getIdxIs).forEach(o -> {
            Map<String, Object> map = new HashMap<>();
            map.put("coloumnParam", String.join(",", colNameList));
            if (data.getRouteFlag()) {
                map.put("tableName", "{" + o.getTableName() + "}");
            } else {
                map.put("tableName", o.getTableName());
            }
            map.put("idxColumnName", o.getColumnName());
            String outPath = data.getDaoImplPath() + "//SEL_" + o.getTableName() + "_BY_" + o.getColumnName() + ".sql";
            FreemarkerBuilder.build(map, "selectSql.ftl", outPath);
        });
    }

    private static void buildColumnList(List<ColumnInfo> columnList, DataBean data) {
        List<ColumnInfoBean> columnInfoBeans = new ArrayList<>();
        for (ColumnInfo info : columnList) {
            ColumnInfoBean infoBean = new ColumnInfoBean();
            infoBean.setColumnName(info.columnName);
            infoBean.setKeyIs(info.keyIs);
            infoBean.setIdxIs(info.idxIs);
            infoBean.setFirstUpperName(info.firstUpperName);
            infoBean.setFirstLowerName(info.firstLowerName);
            infoBean.setColumnTypeName(info.columnTypeName);
            infoBean.setBoDataType(info.boDataType);
            infoBean.setTableName(info.tableName);
            columnInfoBeans.add(infoBean);
        }
        data.setColumnInfos(columnInfoBeans);
    }


    private static void genBean(BoCtrlInfo ctrl, List<ColumnInfo> columnList) throws Exception {
        SysLog.log(ctrl.logName, StrUtil.getFormat20Str("genBean") + "[" + ctrl.beanFilePath + "]");

        // 创建目录
        DirUtil.mkDirs(ctrl.beanPath);

        //
        OutputStreamWriter outstream = new OutputStreamWriter(new FileOutputStream(ctrl.beanFilePath), StrUtil.encoding);
        PrintWriter out = new PrintWriter(outstream);

        out.println("package " + ctrl.beanPackage + ";");

        out.println("");
        out.println("import java.sql.Timestamp;");
        out.println("");

        out.println("import com.ai.appframe2.bo.DataContainer;");
        out.println("import com.ai.appframe2.common.AIException;");
        out.println("import com.ai.appframe2.common.DataContainerInterface;");
        out.println("import com.ai.appframe2.common.DataType;");
        out.println("import com.ai.appframe2.common.ObjectType;");
        out.println("import com.ai.appframe2.common.ServiceManager;");
        out.println("import " + ctrl.valuePackage + "" + ctrl.valueClass + ";");

        out.println("");

        out.println("public class " + ctrl.beanClass + " extends DataContainer implements DataContainerInterface, " + ctrl.valueClass);

        out.println("{");

        out.println("    private static String m_boName = \"" + ctrl.boPackage + "" + ctrl.boName + "\";");
        out.println("");

        for (ColumnInfo ci : columnList) {
            out.println("    public final static String " + ci.varName + " = \"" + ci.columnName + "\";");
            out.println("");
        }

        out.println("    public static ObjectType S_TYPE = null;");
        out.println("");

        out.println("    static");
        out.println("    {");
        out.println("        try");
        out.println("        {");
        out.println("            S_TYPE = ServiceManager.getObjectTypeFactory().getInstance(m_boName);");
        out.println("        }");
        out.println("        catch (Exception e)");
        out.println("        {");
        out.println("            throw new RuntimeException(e);");
        out.println("        }");
        out.println("    }");
        out.println("");
        out.println("    public " + ctrl.beanClass + "() throws AIException");
        out.println("    {");
        out.println("        super(S_TYPE);");
        out.println("    }");
        out.println("");
        out.println("    public static ObjectType getObjectTypeStatic() throws AIException");
        out.println("    {");
        out.println("        return S_TYPE;");
        out.println("    }");
        out.println("");
        out.println("    @Override");
        out.println("    public void setObjectType(ObjectType value) throws AIException");
        out.println("    {");
        out.println("        //");
        out.println("        throw new AIException(\"Cannot reset ObjectType\");");
        out.println("    }");
        out.println("");

        for (ColumnInfo ci : columnList) {
            // init
            out.println("    public void init" + ci.firstUpperName + "(" + getParmType(ci.boDataType, ci.varName) + " value)");
            out.println("    {");
            out.println("        this.initProperty(" + ci.varName + ", " + getInitValue(ci.boDataType, "value") + ");");
            out.println("    }");
            out.println("");

            // set
            out.println("    public void set" + ci.firstUpperName + "(" + getParmType(ci.boDataType, ci.varName) + " value)");
            out.println("    {");
            out.println("        this.set(" + ci.varName + ", " + getInitValue(ci.boDataType, "value") + ");");
            out.println("    }");
            out.println("");

            // set null
            out.println("    public void set" + ci.firstUpperName + "Null()");
            out.println("    {");
            out.println("        this.set(" + ci.varName + ", null);");
            out.println("    }");
            out.println("");

            // get
            out.println("    public " + getParmType(ci.boDataType, "abc") + " get" + ci.firstUpperName + "()");
            out.println("    {");
            out.println("        " + getDataTypeAs(ci.boDataType, ci.varName) + ";");
            out.println("    }");
            out.println("");

            // get init
            out.println("    public " + getParmType(ci.boDataType, "abc") + " get" + ci.firstUpperName + "InitialValue()");
            out.println("    {");
            out.println("        " + getOldDataTypeAs(ci.boDataType, ci.varName) + ";");
            out.println("    }");
            out.println("");
        }

        out.println("}");

        out.close();
    }

    private static void genBo(BoCtrlInfo ctrl, List<ColumnInfo> columnList) throws Exception {
        SysLog.log(ctrl.logName, StrUtil.getFormat20Str("genBo") + "[" + ctrl.boFilePath + "]");

        // 创建目录
        DirUtil.mkDirs(ctrl.boPath);

        //
        OutputStreamWriter outstream = new OutputStreamWriter(new FileOutputStream(ctrl.boFilePath), StrUtil.encoding);
        PrintWriter out = new PrintWriter(outstream);

        out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        out.println("<sysbolist>");
        out.println(" <sysbo datasource=\"" + ctrl.dbname + "\" name=\"" + ctrl.boName + "\" mainattr=\"" + getBoCfgMainAttr(ctrl) + "\">");

        String tablename;

        if (ctrl.xlsctrlinfo.ispart == true) {
            tablename = "{" + ctrl.xlsctrlinfo.tableName + "}";
        } else {
            tablename = ctrl.xlsctrlinfo.tableName;
        }

        out.println("  <mapingenty type=\"table\">" + tablename + "</mapingenty>");
        out.println("  <datafilter />");
        out.println("  <attrlist>");

        for (ColumnInfo ci : columnList) {
            String utf8Str = "";
            if (StringUtils.isNotBlank(ci.columnRemark)) {
                utf8Str = URLDecoder.decode(ci.columnRemark, "UTF-8");
            }
            out.println("   <attr floatlength=\"" + getBoCfgFloatLen(ci) + "\"" + " name=" + "\"" + ci.columnName + "\" maxlength=\"" + getBoCfgPrecision(ci) + "\" datatype=\"" + ci.boDataType + "\" type=\"" + getBoCfgColType(ci) + "\" remark=\""
                    + utf8Str + "\">");
            out.println("    <mapingcol datatype=\"" + ci.columnTypeName + "\">" + ci.columnName + "</mapingcol>");
            out.println("   </attr>");
        }

        out.println("    </attrlist>");
        out.println("  </sysbo>");
        out.println("</sysbolist>");

        out.close();
    }

    public static void genDaoSvCtrlInfo(BoCtrlInfo ctrl) throws Exception {
        PrintWriter out = null;

        String filepath;

        Iterator<String> ait;
        List<String> list;
        String key;

        //
        ait = ctrl.daosvFwMap.keySet().iterator();

        //
        SysLog.log(ctrl.logName, "");
        SysLog.log(ctrl.logName, "数据写dao/sv文件===>[开始]");

        while (ait.hasNext()) {
            filepath = ait.next();

            out = ctrl.daosvFwMap.get(filepath);

            SysLog.log(ctrl.logName, StrUtil.getFormat20Str("genDaoSvCtrlInfo") + "[" + filepath + "]");

            // ====================
            // 1
            key = filepath + "-" + DaoScType.dao_c1ib;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                out.println("");
                out.println("import java.sql.Timestamp;");
                out.println("");

                for (String line : list) {
                    out.println(line);
                }

                out.println("");
            }

            // 2
            key = filepath + "-" + DaoScType.dao_c1p;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                for (String line : list) {
                    out.println(line);
                }

                out.println("{");
            }

            // 3
            key = filepath + "-" + DaoScType.dao_c1m;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                boolean newline = false;

                for (String line : list) {
                    if (newline) {
                        out.println();
                    }
                    newline = true;

                    out.println(line + ";");
                }

                out.println("}");
            }

            // ====================
            // 1
            key = filepath + "-" + DaoScType.dao_c2ib;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                out.println("");
                out.println("import java.sql.Timestamp;");
                out.println("");

                for (String line : list) {
                    out.println(line);
                }
            }

            key = filepath + "-" + DaoScType.dao_c2ie;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                for (String line : list) {
                    out.println(line);
                }

            }

            key = filepath + "-" + DaoScType.dao_c2ii;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                for (String line : list) {
                    out.println(line);
                }

                out.println("");
            }

            // 2
            key = filepath + "-" + DaoScType.dao_c2p;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                for (String line : list) {
                    out.println(line);
                }

                out.println("{");
            }

            // 3
            key = filepath + "-" + DaoScType.dao_c2m;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                boolean newline = false;

                for (String line : list) {
                    if (newline) {
                        out.println();
                    }
                    newline = true;

                    out.println(line);
                }

                out.println("}");
            }

            // ====================
            // 1
            key = filepath + "-" + DaoScType.dao_c3iv;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                out.println("");
                out.println("import java.sql.Timestamp;");
                out.println("");

                for (String line : list) {
                    out.println(line);
                }

                out.println("");
            }

            // 2
            key = filepath + "-" + DaoScType.dao_c3p;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                for (String line : list) {
                    out.println(line);
                }

                out.println("{");
            }

            // 3
            key = filepath + "-" + DaoScType.dao_c3m;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                boolean newline = false;

                for (String line : list) {
                    if (newline) {
                        out.println();
                    }
                    newline = true;

                    out.println(line + ";");
                }

                out.println("}");
            }

            // ====================
            // 1
            key = filepath + "-" + DaoScType.dao_c4ie;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                out.println("");
                out.println("import java.sql.Timestamp;");
                out.println("");

                for (String line : list) {
                    out.println(line);
                }

            }

            key = filepath + "-" + DaoScType.dao_c4iv;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                for (String line : list) {
                    out.println(line);
                }
            }

            key = filepath + "-" + DaoScType.dao_c4ii;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                for (String line : list) {
                    out.println(line);
                }

                out.println("");
            }

            // 2
            key = filepath + "-" + DaoScType.dao_c4p;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                for (String line : list) {
                    out.println(line);
                }

                out.println("{");
            }

            // 3
            key = filepath + "-" + DaoScType.dao_c4m;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                boolean newline = false;

                for (String line : list) {
                    if (newline) {
                        out.println();
                    }
                    newline = true;

                    out.println(line);
                }

                out.println("}");
            }

            // ====================
            // 1
            key = filepath + "-" + DaoScType.sv_c1ib;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                out.println("");
                out.println("import java.sql.Timestamp;");
                out.println("");

                for (String line : list) {
                    out.println(line);
                }

                out.println("");
            }

            // 2
            key = filepath + "-" + DaoScType.sv_c1p;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                for (String line : list) {
                    out.println(line);
                }

                out.println("{");
            }

            // 3
            key = filepath + "-" + DaoScType.sv_c1m;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                boolean newline = false;

                for (String line : list) {
                    if (newline) {
                        out.println();
                    }
                    newline = true;

                    out.println(line + ";");
                }

                out.println("}");
            }

            // ====================
            // 1
            key = filepath + "-" + DaoScType.sv_c2ib;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                out.println("");
                out.println("import java.sql.Timestamp;");
                out.println("");
                out.println("import com.ai.appframe2.service.ServiceFactory;");

                for (String line : list) {
                    out.println(line);
                }
            }

            key = filepath + "-" + DaoScType.sv_c2id;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                for (String line : list) {
                    out.println(line);
                }

            }

            key = filepath + "-" + DaoScType.sv_c2ii;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                for (String line : list) {
                    out.println(line);
                }

                out.println("");
            }

            // 2
            key = filepath + "-" + DaoScType.sv_c2p;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                for (String line : list) {
                    out.println(line);
                }

                out.println("{");
            }

            // 3
            key = filepath + "-" + DaoScType.sv_c2m;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                boolean newline = false;

                for (String line : list) {
                    if (newline) {
                        out.println();
                    }
                    newline = true;

                    out.println(line);
                }

                out.println("}");
            }

            // ====================
            // 1
            key = filepath + "-" + DaoScType.sv_c3iv;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                out.println("");
                out.println("import java.sql.Timestamp;");
                out.println("import java.util.List;");
                out.println("");

                for (String line : list) {
                    out.println(line);
                }

                out.println("");
            }

            // 2
            key = filepath + "-" + DaoScType.sv_c3p;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                for (String line : list) {
                    out.println(line);
                }

                out.println("{");
            }

            // 3
            key = filepath + "-" + DaoScType.sv_c3m;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                boolean newline = false;

                for (String line : list) {
                    if (newline) {
                        out.println();
                    }
                    newline = true;

                    out.println(line + ";");
                }

                out.println("}");
            }

            // ====================
            // 1
            key = filepath + "-" + DaoScType.sv_c4id;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                out.println("");
                out.println("import java.sql.Timestamp;");
                out.println("import java.util.List;");
                out.println("");
                out.println("import com.ai.appframe2.service.ServiceFactory;");

                for (String line : list) {
                    out.println(line);
                }

            }

            key = filepath + "-" + DaoScType.sv_c4iv;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                for (String line : list) {
                    out.println(line);
                }
            }

            key = filepath + "-" + DaoScType.sv_c4ii;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                for (String line : list) {
                    out.println(line);
                }

                out.println("");
            }

            // 2
            key = filepath + "-" + DaoScType.sv_c4p;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                for (String line : list) {
                    out.println(line);
                }

                out.println("{");
            }

            // 3
            key = filepath + "-" + DaoScType.sv_c4m;

            list = ctrl.daosvSrcMap.get(key);

            if (list != null) {
                boolean newline = false;

                for (String line : list) {
                    if (newline) {
                        out.println();
                    }
                    newline = true;

                    out.println(line);
                }

                out.println("}");
            }

            out.close();
        }

        SysLog.log(ctrl.logName, "数据写dao/sv文件===>[结束]");
    }

    private static void genIDao(BoCtrlInfo ctrl, DataBean data) throws Exception {
        SysLog.log(ctrl.logName, StrUtil.getFormat20Str("genIDao") + "[" + data.getDaoFilePath() + "]");

        // 创建目录
        DirUtil.mkDirs(data.getDaoPath());
        FreemarkerBuilder.build(data, "dao.ftl", data.getDaoFilePath());
    }

    private static void genIDaoImpl(BoCtrlInfo ctrl, DataBean data) throws Exception {
        SysLog.log(ctrl.logName, StrUtil.getFormat20Str("genIDao") + "[" + data.getDaoImplFilePath() + "]");
        DirUtil.mkDirs(data.getDaoImplPath());
        FreemarkerBuilder.build(data, "daoImpl.ftl", data.getDaoImplFilePath());
    }


    private static void genEngine2(BoCtrlInfo ctrl, List<ColumnInfo> columnList) throws Exception {
        SysLog.log(ctrl.logName, StrUtil.getFormat20Str("genEngine") + "[" + ctrl.engineFilePath + "]");

        // 创建目录
        DirUtil.mkDirs(ctrl.enginePath);

        Configuration cfg = new Configuration();

        cfg.setClassForTemplateLoading(GenBoFile.class, "/");
        Template t = cfg.getTemplate("freemarker/boengine.ftl");
        FileOutputStream fos = new FileOutputStream(ctrl.engineFilePath);

        Map data = new HashMap();
        data.put("enginePakage", ctrl.enginePakage);
        data.put("valuePackage", ctrl.valuePackage);
        data.put("valueClass", ctrl.valueClass);
        data.put("engineClass", ctrl.engineClass);
        data.put("beanClass", ctrl.beanClass);

        if (ctrl.keyCnt > 0) {
            for (int i = 1; i <= ctrl.keyCnt; i++) {
                ColumnInfo keyci = ctrl.keyList.get(i);
                String type = getParmType(keyci.boDataType, "");
                String var = "_" + keyci.firstUpperName;
                String parm = type + " " + var;
                if (ctrl.paramStr.length() != 0) {
                    ctrl.paramStr = ctrl.paramStr + ", " + parm;
                } else {
                    ctrl.paramStr = parm;
                }
                if (ctrl.paramValue.length() != 0) {
                    ctrl.paramValue = ctrl.paramValue + ", " + var;
                } else {
                    ctrl.paramValue = var;
                }
                data.put("paramStr", ctrl.paramStr);
            }

            String condStr = "";
            for (int i = 1; i <= ctrl.keyCnt; i++) {
                ColumnInfo keyci = ctrl.keyList.get(i);

                String key = ":S_" + keyci.columnName;
                key = key.toUpperCase();

                String parm = keyci.columnName + " = " + key;

                if (condStr.length() != 0) {
                    condStr = condStr + " and " + parm;
                } else {
                    condStr = parm;
                }
            }
            data.put("condStr", condStr);

            List keyList = new ArrayList();
            for (int i = 1; i <= ctrl.keyCnt; i++) {
                Map keyMap = new HashMap();
                ColumnInfo keyci = ctrl.keyList.get(i);

                String key = "S_" + keyci.columnName;
                key = key.toUpperCase();

                String value = "_" + keyci.firstUpperName;
                value = getInitValue(keyci.boDataType, value);
                keyMap.put("key", key);
                keyMap.put("value", value);
                keyList.add(keyMap);
            }
            data.put("keyList", keyList);
        }

        t.process(data, new OutputStreamWriter(fos, StrUtil.encoding));
        fos.flush();
        fos.close();
    }

    private static void genEngine(BoCtrlInfo ctrl, List<ColumnInfo> columnList) throws Exception {
        SysLog.log(ctrl.logName, StrUtil.getFormat20Str("genEngine") + "[" + ctrl.engineFilePath + "]");

        // 创建目录
        DirUtil.mkDirs(ctrl.enginePath);

        //
        OutputStreamWriter outstream = new OutputStreamWriter(new FileOutputStream(ctrl.engineFilePath), StrUtil.encoding);
        PrintWriter out = new PrintWriter(outstream);

        out.println("package " + ctrl.enginePakage + ";");
        out.println("");

        out.println("import java.sql.Connection;");
        out.println("import java.sql.ResultSet;");
        out.println("import java.sql.Timestamp;");
        out.println("import java.util.HashMap;");
        out.println("import java.util.Map;");
        out.println("");
        out.println("import " + ctrl.valuePackage + "" + ctrl.valueClass + ";");
        out.println("import com.ai.appframe2.bo.DataContainerFactory;");
        out.println("import com.ai.appframe2.common.AIException;");
        out.println("import com.ai.appframe2.common.DataContainerInterface;");
        out.println("import com.ai.appframe2.common.ServiceManager;");
        out.println("import com.ai.appframe2.util.criteria.Criteria;");

        out.println("");

        out.println("public class " + ctrl.engineClass);
        out.println("{");
        out.println("");
        out.println("    public static " + ctrl.beanClass + "[] getBeans(DataContainerInterface dc) throws Exception");
        out.println("    {");
        out.println("        Map ps = dc.getProperties();");
        out.println("        StringBuilder builder = new StringBuilder();");
        out.println("        Map pList = new HashMap();");
        out.println("        for (java.util.Iterator cc = ps.entrySet().iterator(); cc.hasNext();)");
        out.println("        {");
        out.println("            Map.Entry e = (Map.Entry) cc.next();");
        out.println("            if (builder.length() > 0)");
        out.println("            {");
        out.println("                builder.append(\" and \");");
        out.println("            }");
        out.println("            builder.append(e.getKey().toString() + \" = :p_\" + e.getKey().toString());");
        out.println("            pList.put(\"p_\" + e.getKey().toString(), e.getValue());");
        out.println("        }");
        out.println("        Connection conn = ServiceManager.getSession().getConnection();");
        out.println("        try");
        out.println("        {");
        out.println("            return getBeans(builder.toString(), pList);");
        out.println("        }");
        out.println("        finally");
        out.println("        {");
        out.println("            if (conn != null)");
        out.println("            {");
        out.println("                conn.close();");
        out.println("            }");
        out.println("        }");
        out.println("    }");
        out.println("");

        ctrl.paramStr = "";
        ctrl.paramValue = "";

        if (ctrl.keyCnt > 0) {

            for (int i = 1; i <= ctrl.keyCnt; i++) {
                ColumnInfo keyci = ctrl.keyList.get(i);

                String type = getParmType(keyci.boDataType, "");

                String var = "_" + keyci.firstUpperName;

                String parm = type + " " + var;

                if (ctrl.paramStr.length() != 0) {
                    ctrl.paramStr = ctrl.paramStr + ", " + parm;
                } else {
                    ctrl.paramStr = parm;
                }

                if (ctrl.paramValue.length() != 0) {
                    ctrl.paramValue = ctrl.paramValue + ", " + var;
                } else {
                    ctrl.paramValue = var;
                }
            }

            out.println("    public static " + ctrl.beanClass + " getBean(" + ctrl.paramStr + ") throws Exception");
            out.println("    {");
            out.println("        /** new create */");

            String condStr = "";

            for (int i = 1; i <= ctrl.keyCnt; i++) {
                ColumnInfo keyci = ctrl.keyList.get(i);

                String key = ":S_" + keyci.columnName;
                key = key.toUpperCase();

                String parm = keyci.columnName + " = " + key;

                if (condStr.length() != 0) {
                    condStr = condStr + " and " + parm;
                } else {
                    condStr = parm;
                }
            }

            out.println("        String condition = \"" + condStr + "\";");
            out.println("        Map map = new HashMap();");

            for (int i = 1; i <= ctrl.keyCnt; i++) {
                ColumnInfo keyci = ctrl.keyList.get(i);

                String key = "S_" + keyci.columnName;
                key = key.toUpperCase();

                String value = "_" + keyci.firstUpperName;
                value = getInitValue(keyci.boDataType, value);

                out.println("        map.put(\"" + key + "\", " + value + ");");
            }

            out.println("");
            out.println("        " + ctrl.beanClass + "[] beans = getBeans(condition, map);");
            out.println("        if (beans != null && beans.length == 1)");
            out.println("        {");
            out.println("            return beans[0];");
            out.println("        }");
            out.println("        else if (beans != null && beans.length > 1)");
            out.println("        {");
            out.println("            // [错误]根据主键查询出现一条以上记录");
            out.println("            throw new Exception(\"[ERROR]More datas than one queryed by PK\");");
            out.println("        }");
            out.println("        else");
            out.println("        {");

            // out.println(" " + ctrl.beanClass + " bean = new " + ctrl.beanClass + "();");
            // for (int i = 1; i <= ctrl.keyCnt; i++)
            // {
            // ColumnInfo keyci = ctrl.keyList.get(i);
            //
            // String method = "set" + keyci.firstUpperName;
            //
            // String var = "_" + keyci.firstUpperName;
            //
            // out.println(" bean." + method + "(" + var + ");");
            // }
            // out.println(" return bean;");
            out.println("            return null;");

            out.println("        }");
            out.println("    }");
            out.println("");
        }

        out.println("    public static " + ctrl.beanClass + "[] getBeans(Criteria sql) throws Exception");
        out.println("    {");
        out.println("        return getBeans(sql, -1, -1, false);");
        out.println("    }");
        out.println("");
        out.println("    public static " + ctrl.beanClass + "[] getBeans(Criteria sql, int startNum, int endNum, boolean isShowFK) throws Exception");
        out.println("    {");
        out.println("        String[] cols = null;");
        out.println("        String condition = \"\";");
        out.println("        Map param = null;");
        out.println("        if (sql != null)");
        out.println("        {");
        out.println("            cols = (String[]) sql.getSelectColumns().toArray(new String[0]);");
        out.println("            condition = sql.toString();");
        out.println("            param = sql.getParameters();");
        out.println("        }");
        out.println("        return getBeans(cols, condition, param, startNum, endNum, isShowFK);");
        out.println("    }");
        out.println("");
        out.println("    public static " + ctrl.beanClass + "[] getBeans(String condition, Map parameter) throws Exception");
        out.println("    {");
        out.println("        return getBeans(null, condition, parameter, -1, -1, false);");
        out.println("    }");
        out.println("");
        out.println("    public static " + ctrl.beanClass + "[] getBeans(String[] cols, String condition, Map parameter, int startNum, int endNum, boolean isShowFK) throws Exception");
        out.println("    {");
        out.println("        Connection conn = null;");
        out.println("        try");
        out.println("        {");
        out.println("            conn = ServiceManager.getSession().getConnection();");
        out.println("            return (" + ctrl.beanClass + "[]) ServiceManager.getDataStore().retrieve(conn, " + ctrl.beanClass + ".class, " + ctrl.beanClass
                + ".getObjectTypeStatic(), cols, condition, parameter, startNum, endNum, isShowFK, false, null);");
        out.println("        }");
        out.println("        catch (Exception e)");
        out.println("        {");
        out.println("            throw e;");
        out.println("        }");
        out.println("        finally");
        out.println("        {");
        out.println("            if (conn != null)");
        out.println("            {");
        out.println("                conn.close();");
        out.println("            }");
        out.println("        }");
        out.println("    }");
        out.println("");
        out.println("    public static " + ctrl.beanClass + "[] getBeans(String[] cols, String condition, Map parameter, int startNum, int endNum, boolean isShowFK, String[] extendBOAttrs) throws Exception");
        out.println("    {");
        out.println("        Connection conn = null;");
        out.println("        try");
        out.println("        {");
        out.println("            conn = ServiceManager.getSession().getConnection();");
        out.println("            return (" + ctrl.beanClass + "[]) ServiceManager.getDataStore().retrieve(conn, " + ctrl.beanClass + ".class, " + ctrl.beanClass
                + ".getObjectTypeStatic(), cols, condition, parameter, startNum, endNum, isShowFK, false, extendBOAttrs);");
        out.println("        }");
        out.println("        catch (Exception e)");
        out.println("        {");
        out.println("            throw e;");
        out.println("        }");
        out.println("        finally");
        out.println("        {");
        out.println("            if (conn != null)");
        out.println("            {");
        out.println("                conn.close();");
        out.println("            }");
        out.println("        }");
        out.println("    }");
        out.println("");
        out.println("    public static int getBeansCount(String condition, Map parameter) throws Exception");
        out.println("    {");
        out.println("        Connection conn = null;");
        out.println("        try");
        out.println("        {");
        out.println("            conn = ServiceManager.getSession().getConnection();");
        out.println("            return ServiceManager.getDataStore().retrieveCount(conn, " + ctrl.beanClass + ".getObjectTypeStatic(), condition, parameter, null);");
        out.println("        }");
        out.println("        catch (Exception e)");
        out.println("        {");
        out.println("            throw e;");
        out.println("        }");
        out.println("        finally");
        out.println("        {");
        out.println("            if (conn != null)");
        out.println("            {");
        out.println("                conn.close();");
        out.println("            }");
        out.println("        }");
        out.println("    }");
        out.println("");
        out.println("    public static int getBeansCount(String condition, Map parameter, String[] extendBOAttrs) throws Exception");
        out.println("    {");
        out.println("        Connection conn = null;");
        out.println("        try");
        out.println("        {");
        out.println("            conn = ServiceManager.getSession().getConnection();");
        out.println("            return ServiceManager.getDataStore().retrieveCount(conn, " + ctrl.beanClass + ".getObjectTypeStatic(), condition, parameter, extendBOAttrs);");
        out.println("        }");
        out.println("        catch (Exception e)");
        out.println("        {");
        out.println("            throw e;");
        out.println("        }");
        out.println("        finally");
        out.println("        {");
        out.println("            if (conn != null)");
        out.println("            {");
        out.println("                conn.close();");
        out.println("            }");
        out.println("        }");
        out.println("    }");
        out.println("");
        out.println("    public static void save(" + ctrl.beanClass + " aBean) throws Exception");
        out.println("    {");
        out.println("        Connection conn = null;");
        out.println("        try");
        out.println("        {");
        out.println("            conn = ServiceManager.getSession().getConnection();");
        out.println("            ServiceManager.getDataStore().save(conn, aBean);");
        out.println("        }");
        out.println("        catch (Exception e)");
        out.println("        {");
        out.println("            throw e;");
        out.println("        }");
        out.println("        finally");
        out.println("        {");
        out.println("            if (conn != null)");
        out.println("            {");
        out.println("                conn.close();");
        out.println("            }");
        out.println("        }");
        out.println("    }");
        out.println("");
        out.println("    public static void save(" + ctrl.beanClass + "[] aBeans) throws Exception");
        out.println("    {");
        out.println("        Connection conn = null;");
        out.println("        try");
        out.println("        {");
        out.println("            conn = ServiceManager.getSession().getConnection();");
        out.println("            ServiceManager.getDataStore().save(conn, aBeans);");
        out.println("        }");
        out.println("        catch (Exception e)");
        out.println("        {");
        out.println("            throw e;");
        out.println("        }");
        out.println("        finally");
        out.println("        {");
        out.println("            if (conn != null)");
        out.println("            {");
        out.println("                conn.close();");
        out.println("            }");
        out.println("        }");
        out.println("    }");
        out.println("");
        out.println("    public static void saveBatch(" + ctrl.beanClass + "[] aBeans) throws Exception");
        out.println("    {");
        out.println("        Connection conn = null;");
        out.println("        try");
        out.println("        {");
        out.println("            conn = ServiceManager.getSession().getConnection();");
        out.println("            ServiceManager.getDataStore().saveBatch(conn, aBeans);");
        out.println("        }");
        out.println("        catch (Exception e)");
        out.println("        {");
        out.println("            throw e;");
        out.println("        }");
        out.println("        finally");
        out.println("        {");
        out.println("            if (conn != null)");
        out.println("            {");
        out.println("                conn.close();");
        out.println("            }");
        out.println("        }");
        out.println("    }");
        out.println("");
        out.println("    public static " + ctrl.beanClass + "[] getBeansFromQueryBO(String soureBO, Map parameter) throws Exception");
        out.println("    {");
        out.println("        Connection conn = null;");
        out.println("        ResultSet resultset = null;");
        out.println("        try");
        out.println("        {");
        out.println("            conn = ServiceManager.getSession().getConnection();");
        out.println("            String sql = ServiceManager.getObjectTypeFactory().getInstance(soureBO).getMapingEnty();");
        out.println("            resultset = ServiceManager.getDataStore().retrieve(conn, sql, parameter);");
        out.println("            return (" + ctrl.beanClass + "[]) ServiceManager.getDataStore().crateDtaContainerFromResultSet(" + ctrl.beanClass + ".class, " + ctrl.beanClass + ".getObjectTypeStatic(), resultset, null, true);");
        out.println("        }");
        out.println("        catch (Exception e)");
        out.println("        {");
        out.println("            throw e;");
        out.println("        }");
        out.println("        finally");
        out.println("        {");
        out.println("            if (resultset != null)");
        out.println("            {");
        out.println("                resultset.close();");
        out.println("            }");
        out.println("            if (conn != null)");
        out.println("            {");
        out.println("                conn.close();");
        out.println("            }");
        out.println("        }");
        out.println("    }");
        out.println("");
        out.println("    public static " + ctrl.beanClass + "[] getBeansFromSql(String sql, Map parameter) throws Exception");
        out.println("    {");
        out.println("        Connection conn = null;");
        out.println("        ResultSet resultset = null;");
        out.println("        try");
        out.println("        {");
        out.println("            conn = ServiceManager.getSession().getConnection();");
        out.println("            resultset = ServiceManager.getDataStore().retrieve(conn, sql, parameter);");
        out.println("            return (" + ctrl.beanClass + "[]) ServiceManager.getDataStore().crateDtaContainerFromResultSet(" + ctrl.beanClass + ".class, " + ctrl.beanClass + ".getObjectTypeStatic(), resultset, null, true);");
        out.println("        }");
        out.println("        catch (Exception e)");
        out.println("        {");
        out.println("            throw e;");
        out.println("        }");
        out.println("        finally");
        out.println("        {");
        out.println("            if (resultset != null)");
        out.println("            {");
        out.println("                resultset.close();");
        out.println("            }");
        out.println("            if (conn != null)");
        out.println("            {");
        out.println("                conn.close();");
        out.println("            }");
        out.println("        }");
        out.println("    }");
        out.println("");
        out.println("    public static java.math.BigDecimal getNewId() throws Exception");
        out.println("    {");
        out.println("        return ServiceManager.getIdGenerator().getNewId(" + ctrl.beanClass + ".getObjectTypeStatic());");
        out.println("    }");
        out.println("");
        out.println("    public static java.sql.Timestamp getSysDate() throws Exception");
        out.println("    {");
        out.println("        return ServiceManager.getIdGenerator().getSysDate(" + ctrl.beanClass + ".getObjectTypeStatic());");
        out.println("    }");
        out.println("");
        out.println("    public static " + ctrl.beanClass + " wrap(DataContainerInterface source, Map colMatch, boolean canModify)");
        out.println("    {");
        out.println("        try");
        out.println("        {");
        out.println("            return (" + ctrl.beanClass + ") DataContainerFactory.wrap(source, " + ctrl.beanClass + ".class, colMatch, canModify);");
        out.println("        }");
        out.println("        catch (Exception e)");
        out.println("        {");
        out.println("            if (e.getCause() != null)");
        out.println("            {");
        out.println("                throw new RuntimeException(e.getCause());");
        out.println("            }");
        out.println("            else");
        out.println("            {");
        out.println("                throw new RuntimeException(e);");
        out.println("            }");
        out.println("        }");
        out.println("    }");
        out.println("");
        out.println("    public static " + ctrl.beanClass + " copy(DataContainerInterface source, Map colMatch, boolean canModify)");
        out.println("    {");
        out.println("        try");
        out.println("        {");
        out.println("            " + ctrl.beanClass + " result = new " + ctrl.beanClass + "();");
        out.println("            DataContainerFactory.copy(source, result, colMatch);");
        out.println("            return result;");
        out.println("        }");
        out.println("        catch (AIException ex)");
        out.println("        {");
        out.println("            if (ex.getCause() != null)");
        out.println("            {");
        out.println("                throw new RuntimeException(ex.getCause());");
        out.println("            }");
        out.println("            else");
        out.println("            {");
        out.println("                throw new RuntimeException(ex);");
        out.println("            }");
        out.println("        }");
        out.println("    }");
        out.println("");
        out.println("    public static " + ctrl.beanClass + " transfer(" + ctrl.valueClass + " value)");
        out.println("    {");
        out.println("        if (value == null)");
        out.println("        {");
        out.println("            return null;");
        out.println("        }");
        out.println("        try");
        out.println("        {");
        out.println("            if (value instanceof " + ctrl.beanClass + ")");
        out.println("            {");
        out.println("                return (" + ctrl.beanClass + ") value;");
        out.println("            }");
        out.println("            " + ctrl.beanClass + " newBean = new " + ctrl.beanClass + "();");
        out.println("");
        out.println("            DataContainerFactory.transfer(value, newBean);");
        out.println("            return newBean;");
        out.println("        }");
        out.println("        catch (Exception ex)");
        out.println("        {");
        out.println("            if (ex.getCause() != null)");
        out.println("            {");
        out.println("                throw new RuntimeException(ex.getCause());");
        out.println("            }");
        out.println("            else");
        out.println("            {");
        out.println("                throw new RuntimeException(ex);");
        out.println("            }");
        out.println("        }");
        out.println("    }");
        out.println("");
        out.println("    public static " + ctrl.beanClass + "[] transfer(" + ctrl.valueClass + "[] value)");
        out.println("    {");
        out.println("        if (value == null || value.length == 0)");
        out.println("        {");
        out.println("            return null;");
        out.println("        }");
        out.println("");
        out.println("        try");
        out.println("        {");
        out.println("            if (value instanceof " + ctrl.beanClass + "[])");
        out.println("            {");
        out.println("                return (" + ctrl.beanClass + "[]) value;");
        out.println("            }");
        out.println("            " + ctrl.beanClass + "[] newBeans = new " + ctrl.beanClass + "[value.length];");
        out.println("            for (int i = 0; i < newBeans.length; i++)");
        out.println("            {");
        out.println("                newBeans[i] = new " + ctrl.beanClass + "();");
        out.println("                DataContainerFactory.transfer(value[i], newBeans[i]);");
        out.println("            }");
        out.println("            return newBeans;");
        out.println("        }");
        out.println("        catch (Exception ex)");
        out.println("        {");
        out.println("            if (ex.getCause() != null)");
        out.println("            {");
        out.println("                throw new RuntimeException(ex.getCause());");
        out.println("            }");
        out.println("            else");
        out.println("            {");
        out.println("                throw new RuntimeException(ex);");
        out.println("            }");
        out.println("        }");
        out.println("    }");
        out.println("");
        out.println("    public static void save(" + ctrl.valueClass + " aValue) throws Exception");
        out.println("    {");
        out.println("        save(transfer(aValue));");
        out.println("    }");
        out.println("");
        out.println("    public static void save(" + ctrl.valueClass + "[] aValues) throws Exception");
        out.println("    {");
        out.println("        save(transfer(aValues));");
        out.println("    }");
        out.println("");
        out.println("    public static void saveBatch(" + ctrl.valueClass + "[] aValues) throws Exception");
        out.println("    {");
        out.println("        saveBatch(transfer(aValues));");
        out.println("    }");
        out.println("}");

        out.close();
    }

    private static void genValue(BoCtrlInfo ctrl, List<ColumnInfo> columnList) throws Exception {
        SysLog.log(ctrl.logName, StrUtil.getFormat20Str("genValue") + "[" + ctrl.valueFilePath + "]");

        // 创建目录
        DirUtil.mkDirs(ctrl.valuePath);

        //
        OutputStreamWriter outstream = new OutputStreamWriter(new FileOutputStream(ctrl.valueFilePath), StrUtil.encoding);

        PrintWriter out = new PrintWriter(outstream);

        out.println("package " + ctrl.valuePackage + ";");
        out.println("");
        out.println("import java.sql.Timestamp;");
        out.println("");
        out.println("import com.ai.appframe2.common.DataStructInterface;");
        out.println("");
        out.println("public interface " + ctrl.valueClass + " extends DataStructInterface");
        out.println("{");

        for (ColumnInfo ci : columnList) {
            out.println("    public final static String " + ci.varName + " = \"" + ci.columnName + "\";");
            out.println("");
        }

        for (ColumnInfo ci : columnList) {
            out.println("    public " + getParmType(ci.boDataType, ci.varName) + " get" + ci.firstUpperName + "();");
            out.println("");
        }

        for (ColumnInfo ci : columnList) {
            out.println("    public void set" + ci.firstUpperName + "(" + getParmType(ci.boDataType, ci.varName) + " value);");
            out.println("");
        }

        out.println("}");

        out.close();
    }

    private static String getBoCfgColType(ColumnInfo ci) throws Exception {
        String colType = "COL";

        if (ci.keyIs) {
            colType = "PK";
        }

        return colType;
    }

    private static String getBoCfgFloatLen(ColumnInfo ci) throws Exception {
        String fl = "";

        if (!ci.columnTypeName.equals("DATE")) {
            fl = String.valueOf(ci.colunmScale);
        }

        return fl;
    }

    private static String getBoCfgMainAttr(BoCtrlInfo ctrl) throws Exception {
        String mainAttr = "";

        if (ctrl.keyCnt == 1) {
            mainAttr = ctrl.keyList.get(1).columnName;
        }

        return mainAttr;
    }

    private static String getBoCfgPrecision(ColumnInfo ci) throws Exception {
        String fl = "";

        if (!ci.columnTypeName.equals("DATE")) {
            fl = String.valueOf(ci.colunmPrecision);
        }

        return fl;
    }

    private static void getColumnInfo(BoCtrlInfo ctrl, List<ColumnInfo> columnList) throws Exception {
        Boolean my = false;

        if (my) {
            setColumnInfo(columnList);

            return;
        }

        Connection con = null;// 创建一个数据库连接
        PreparedStatement pre = null;// 创建预编译语句对象，一般都是用这个而不用Statement
        ResultSet result = null;// 创建一个结果集对象

        try {
            DBCtrlInfo db = ctrl.dbMap.get(ctrl.dbname);

            if (db.con == null) {
                Properties props = new Properties();

                props.put("remarksReporting", "true");
                props.put("user", db.user);
                props.put("password", db.password);

                Class.forName("oracle.jdbc.driver.OracleDriver");// 加载Oracle驱动程序

                con = DriverManager.getConnection(db.url, props);// 获取连接

                SysLog.log(ctrl.logName, "数据库连接成功[url=" + db.url + "][user=" + db.user + "][passwd=" + db.password + "]");

                db.con = con;
            } else {
                con = db.con;
            }

            //
            DatabaseMetaData meta = con.getMetaData();

            String mysql;

            mysql = "select * from " + ctrl.xlsctrlinfo.tableName + " where 1 = 2";

            pre = con.prepareStatement(mysql);

            result = pre.executeQuery();

            ResultSetMetaData data = result.getMetaData();

            for (int i = 1; i <= data.getColumnCount(); i++) {
                ColumnInfo ci = new ColumnInfo();

                columnList.add(ci);

                // 获得指定列的列名
                ci.columnName = data.getColumnName(i);

                // 获得指定列的数据类型名
                ci.columnTypeName = data.getColumnTypeName(i);

                // 某列类型的精确度(类型的长度)
                ci.colunmPrecision = data.getPrecision(i);

                // 小数点后的位数
                ci.colunmScale = data.getScale(i);

                // init
                ci.tableName = "";
                ci.columnRemark = "";
                ci.keyIs = false;
                ci.keySeq = -1;
            }

            // 得到表主键
            ctrl.keyCnt = 0;
            ctrl.keyList = new ArrayList<ColumnInfo>();

            ResultSet ids = meta.getPrimaryKeys(null, db.user, ctrl.xlsctrlinfo.tableName);

            ctrl.keyList.add(null);
            ctrl.keyList.add(null);
            ctrl.keyList.add(null);
            ctrl.keyList.add(null);
            ctrl.keyList.add(null);
            ctrl.keyList.add(null);
            ctrl.keyList.add(null);
            ctrl.keyList.add(null);
            ctrl.keyList.add(null);
            ctrl.keyList.add(null);
            ctrl.keyList.add(null);
            ctrl.keyList.add(null);
            ctrl.keyList.add(null);
            ctrl.keyList.add(null);
            ctrl.keyList.add(null);
            ctrl.keyList.add(null);

            while (ids != null && ids.next()) {
                ctrl.keyCnt++;

                String keyName = ids.getString("COLUMN_NAME");
                String keySeq = ids.getString("KEY_SEQ");

                for (ColumnInfo ci : columnList) {
                    if (ci.columnName.equals(keyName)) {
                        ci.keyIs = true;
                        ci.keySeq = Integer.valueOf(keySeq);

                        ctrl.keyList.set(ci.keySeq, ci);
                    }
                }
            }

            ISQLSyntaxAnalyzer analyzer = OracleSQLSyntaxAnalyzer.getInstance();
            Set<String> tables = analyzer.extractTables(mysql);

            for (String tableName : tables) {
                // 得到表注释
                ResultSet rs = meta.getColumns(null, db.user, tableName, "%");

                while (rs.next()) {
                    String colName = rs.getString("COLUMN_NAME");
                    String remark = rs.getString("REMARKS");

                    for (ColumnInfo ci : columnList) {
                        if (ci.columnName.equals(colName)) {
                            ci.tableName = tableName;
                            ci.columnRemark = remark;
                        }
                    }
                }

                // 得到索引信息
                ResultSet indexInfo = meta.getIndexInfo(null, db.user, tableName, false, false);
                while (indexInfo.next()) {
                    String indexName = indexInfo.getString("INDEX_NAME");
                    if (StringUtils.isNotBlank(indexName)) {
                        String oSql = "SELECT t.table_name,t.column_name FROM user_ind_columns t WHERE t.index_name = '" + indexName + "'";
                        pre = con.prepareStatement(oSql);
                        rs = pre.executeQuery();
                        while (rs.next()) {
                            String idxColName = rs.getString("COLUMN_NAME");
                            columnList.stream().filter(o -> o.columnName.equals(idxColName))
                                    .findFirst()
                                    .ifPresent(o -> {
                                        o.idxIs = true;
                                    });
                        }
                    }
                }
            }

        } finally {
            try {
                // 逐一将上面的几个对象关闭，因为不关闭的话会影响性能、并且占用资源
                // 注意关闭的顺序，最后使用的最先关闭
                if (result != null) {
                    result.close();
                }

                if (pre != null) {
                    pre.close();
                }

                // if (con != null)
                // {
                // con.close();
                // }

                // SysLog.log(ctrl.logName, "数据库连接已关闭");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static String getDataTypeAs(String type, String name) {
        if (type == null) {
            return "";
        }

        if (type.equals(BoDataType.IntegerType)) {
            return "return DataType.getAsInt(this.get(" + name + "))";
        } else if (type.equals("short")) {
            return "return DataType.getAsShort(this.get(" + name + "))";
        } else if (type.equals(BoDataType.LongType)) {
            return "return DataType.getAsLong(this.get(" + name + "))";
        } else if (type.equals("float")) {
            return "return DataType.getAsFloat(this.get(" + name + "))";
        } else if (type.equals(BoDataType.DoubleType)) {
            return "return DataType.getAsDouble(this.get(" + name + "))";
        } else if (type.equals(BoDataType.CharType)) {
            return "return DataType.getAsString(this.get(" + name + "))";
        } else if (type.equals(BoDataType.StringType)) {
            return "return DataType.getAsString(this.get(" + name + "))";
        } else if (type.equals("date")) {
            return "return DataType.getAsDate(this.get(" + name + "))";
        } else if (type.equals("time")) {
            return "return DataType.getAsTime(this.get(" + name + "))";
        } else if (type.equals(BoDataType.DateTimeType)) {
            return "return DataType.getAsDateTime(this.get(" + name + "))";
        } else {
            return "return (" + type + ")this.get(" + name + ")";
        }
    }

    public static void getIniFile(String[] args, BoCtrlInfo ctrl, String defFile) throws Exception {
        if (args.length == 0) {
//            File directory = new File("");

//            String nowDir = directory.getAbsolutePath();

            ctrl.iniName = defFile;
        } else {
            ctrl.iniName = args[0];
        }

        File iniFile = new File(ctrl.iniName);

        if (!iniFile.isFile() || !iniFile.exists()) {
            System.out.println("异常退出，配置文件不存在[" + ctrl.iniName + "]");

            throw new Exception("ini file not found");
        }
    }

    private static String getInitValue(String type, String name) {
        if (type == null) {
            return "";
        }

        if (type.equals(BoDataType.IntegerType)) {
            return "new Integer(" + name + ")";
        } else if (type.equals("short")) {
            return "return DataType.getAsShort(this.get(" + name + "))";
        } else if (type.equals(BoDataType.LongType)) {
            return "new Long(" + name + ")";
        } else if (type.equals("float")) {
            return "return DataType.getAsFloat(this.get(" + name + "))";
        } else if (type.equals(BoDataType.DoubleType)) {
            return "new Double(" + name + ")";
        } else if (type.equals(BoDataType.CharType)) {
            return name;
        } else if (type.equals(BoDataType.StringType)) {
            return name;
        } else if (type.equals("date")) {
            return "return DataType.getAsDate(this.get(" + name + "))";
        } else if (type.equals("time")) {
            return "return DataType.getAsTime(this.get(" + name + "))";
        } else if (type.equals(BoDataType.DateTimeType)) {
            return name;
        } else {
            return "return (" + type + ")this.get(" + name + ")";
        }
    }

    private static List<String> getMapList(BoCtrlInfo ctrl, String filepath, String str) throws Exception {
        String key = filepath + "-" + str;

        List<String> list = ctrl.daosvSrcMap.get(key);

        if (list == null) {
            list = new ArrayList<String>();

            ctrl.daosvSrcMap.put(key, list);
        }

        return list;
    }

    private static String getOldDataTypeAs(String type, String name) {
        if (type == null) {
            return "";
        }

        if (type.equals(BoDataType.IntegerType)) {
            return "return DataType.getAsInt(this.getOldObj(" + name + "))";
        } else if (type.equals("short")) {
            return "return DataType.getAsShort(this.getOldObj(" + name + "))";
        } else if (type.equals(BoDataType.LongType)) {
            return "return DataType.getAsLong(this.getOldObj(" + name + "))";
        } else if (type.equals("float")) {
            return "return DataType.getAsFloat(this.getOldObj(" + name + "))";
        } else if (type.equals(BoDataType.DoubleType)) {
            return "return DataType.getAsDouble(this.getOldObj(" + name + "))";
        } else if (type.equals(BoDataType.CharType)) {
            return "return DataType.getAsString(this.getOldObj(" + name + "))";
        } else if (type.equals(BoDataType.StringType)) {
            return "return DataType.getAsString(this.getOldObj(" + name + "))";
        } else if (type.equals("Date")) {
            return "return DataType.getAsDate(this.getOldObj(" + name + "))";
        } else if (type.equals("Time")) {
            return "return DataType.getAsTime(this.getOldObj(" + name + "))";
        } else if (type.equals(BoDataType.DateTimeType)) {
            return "return DataType.getAsDateTime(this.getOldObj(" + name + "))";
        } else {
            return "return (" + type + ")this.getOldObj(" + name + ")";
        }
    }

    private static String getParmType(String type, String name) {
        if (type == null) {
            return "";
        }

        if (type.equals(BoDataType.IntegerType)) {
            return "int";
        } else if (type.equals("short")) {
            return "return DataType.getAsShort(this.get(" + name + "))";
        } else if (type.equals(BoDataType.LongType)) {
            return "long";
        } else if (type.equals("float")) {
            return "return DataType.getAsFloat(this.get(" + name + "))";
        } else if (type.equals(BoDataType.DoubleType)) {
            return "double";
        } else if (type.equals(BoDataType.CharType)) {
            return "String";
        } else if (type.equals(BoDataType.StringType)) {
            return "String";
        } else if (type.equals("date")) {
            return "return DataType.getAsDate(this.get(" + name + "))";
        } else if (type.equals("time")) {
            return "return DataType.getAsTime(this.get(" + name + "))";
        } else if (type.equals(BoDataType.DateTimeType)) {
            return "Timestamp";
        } else {
            return "return (" + type + ")this.get(" + name + ")";
        }
    }

    public static void getXlsFile(String[] args, BoCtrlInfo ctrl, String defFile) throws Exception {
        if (args.length == 0) {
//            File directory = new File("");
//
//            String nowDir = directory.getAbsolutePath();

//            ctrl.xlsName = nowDir + defFile;
            ctrl.xlsName = defFile;
        } else {
            ctrl.xlsName = args[1];
        }

        File iniFile = new File(ctrl.xlsName);

        if (!iniFile.isFile() || !iniFile.exists()) {
            System.out.println("异常退出，配置文件不存在[" + ctrl.xlsName + "]");

            throw new Exception("xls file not found");
        }
    }

    public static void mkdirDaoSvInfo(BoCtrlInfo ctrl) throws Exception {
        PrintWriter out;

        String sz[] =
                {ctrl.xlsctrlinfo.daoQueryItfFilePath, ctrl.xlsctrlinfo.daoQueryImplFilePath, ctrl.xlsctrlinfo.daoOperItfFilePath, ctrl.xlsctrlinfo.daoOperImplFilePath, ctrl.xlsctrlinfo.svQueryItfFilePath, ctrl.xlsctrlinfo.svQueryImplFilePath,
                        ctrl.xlsctrlinfo.svOperItfFilePath, ctrl.xlsctrlinfo.svOperImplFilePath};

        for (String filepath : sz) {
            out = ctrl.daosvFwMap.get(filepath);

            if (out != null) {
                continue;
            }

            String path = StrUtil.getFilePath(filepath);

            SysLog.log(ctrl.logName, StrUtil.getFormat20Str("genDaoQueryFw") + "[" + filepath + "]");

            // 创建目录
            DirUtil.mkDirs(path);

            OutputStreamWriter outstream = new OutputStreamWriter(new FileOutputStream(filepath), StrUtil.encoding);
            out = new PrintWriter(outstream);

            String commPackage = StrUtil.getComPackage(path);

            out.println("package " + commPackage + ";");

            ctrl.daosvFwMap.put(filepath, out);
        }
    }

    public static void readIniFile(BoCtrlInfo ctrl) throws Exception {
        SysLog.log(ctrl.logName, "配置文件[" + ctrl.iniName + "]");

        // 读配置信息
        IniReader reader = new IniReader(ctrl.iniName);

        // isshow
        ctrl.isShow = reader.getValueBool("ctrl", "isshow", false);

        // model home path
        ctrl.modelHomePath = reader.getValue("path", "model_home_path");

        // tablelist
        ctrl.dbList = reader.getValueList("db");

        for (String dbname : ctrl.dbList) {
            DBCtrlInfo db = new DBCtrlInfo();

            // db
            db.dbname = dbname;
            db.url = reader.getValue(dbname, "url");
            db.user = reader.getValue(dbname, "user").toUpperCase();
            db.password = reader.getValue(dbname, "passwd");

            ctrl.dbMap.put(dbname, db);
        }
    }

    public static void readXlsFile(BoCtrlInfo ctrl) throws Exception {
        SysLog.log(ctrl.logName, "表格文件[" + ctrl.xlsName + "]");

        InputStream stream = new FileInputStream(ctrl.xlsName);

        Workbook wb = new XSSFWorkbook(stream);
        int icnt = wb.getNumberOfSheets();

        Sheet sheet;
        String sheetName;

        for (int iIndex = 0; iIndex < icnt; iIndex++) {
            //
            sheet = wb.getSheetAt(iIndex);

            //
            sheetName = sheet.getSheetName();
            sheetName = sheetName.toUpperCase();

            LinkedHashMap<String, XlsCtrlInfo> xlsctrl = new LinkedHashMap<String, XlsCtrlInfo>();

            ctrl.xlsMap.put(sheetName, xlsctrl);

            int i = 0;

            // System.out.println(sheetName);
            for (Row row : sheet) {
                i++;

                if (i == 1) {
                    continue;
                }

                XlsCtrlInfo xlsctrlinfo = new XlsCtrlInfo();

                int j = 0;
                for (Cell cell : row) {
                    j++;

                    if (j == 1) {
                        continue;
                    }

                    String value = cell.getStringCellValue();
                    value = value.trim();
                    // System.out.print(value + " ");

                    if (j == 2) {
                        if (value.indexOf("{") != -1) {
                            xlsctrlinfo.ispart = true;

                            value = value.substring(1, value.length() - 1);
                        }

                        xlsctrlinfo.tableName = value;
                    } else if (j == 3) {
                        xlsctrlinfo.center = value;
                        xlsctrlinfo.center2 = StrUtil.firstUpperName(value);
                    } else if (j == 4) {
                        xlsctrlinfo.funmodel = value;
                        xlsctrlinfo.funmodel2 = StrUtil.firstUpperName(value);
                    }
                }

                xlsctrl.put(xlsctrlinfo.tableName, xlsctrlinfo);

                xlsctrlinfo.subpath = xlsctrlinfo.center + "\\soa\\" + xlsctrlinfo.funmodel + "\\model";
                // System.out.println("");
            }
        }
    }

    private static DataBean setBoCtrlInfo(BoCtrlInfo ctrl) {
        DataBean dataBean = new DataBean();
        dataBean.setRouteFlag(true);
        String homePath = ctrl.modelHomePath + "\\" + ctrl.xlsctrlinfo.subpath;

        // bo
        ctrl.boFile = ctrl.boName + ".bo";
        ctrl.boPath = homePath + "\\bo";
        ctrl.boFilePath = ctrl.boPath + "\\" + ctrl.boFile;
        ctrl.boPackage = StrUtil.getComPackage(ctrl.boPath);
        ctrl.boPackage = "com.asiainfo.order.soa.repository.bo";
        dataBean.setBoName(ctrl.boName);
        dataBean.setBoFile(ctrl.boFile);
        dataBean.setBoPath(ctrl.boPath);
        dataBean.setBoFilePath(ctrl.boFilePath);
        dataBean.setBoPackage(ctrl.boPackage);

        // mboname
        ctrl.m_boName = ctrl.boFilePath + "" + ctrl.boName;

        // bean
        ctrl.beanClass = ctrl.boName + "Bean";
        ctrl.beanFile = ctrl.beanClass + ".java";
        ctrl.beanPath = homePath + "\\bo";
        ctrl.beanFilePath = ctrl.beanPath + "\\" + ctrl.beanFile;
        ctrl.beanPackage = StrUtil.getComPackage(ctrl.beanPath);
        ctrl.beanPackage = "com.asiainfo.order.soa.repository.bo";
        dataBean.setBeanClass(ctrl.beanClass);
        dataBean.setBeanFile(ctrl.beanFile);
        dataBean.setBeanPath(ctrl.beanPath);
        dataBean.setBeanFilePath(ctrl.beanFilePath);
        dataBean.setBeanPackage(ctrl.beanPackage);

        // engine
        ctrl.engineClass = ctrl.boName + "Engine";
        ctrl.engineFile = ctrl.engineClass + ".java";
        ctrl.enginePath = homePath + "\\bo";
        ctrl.engineFilePath = ctrl.enginePath + "\\" + ctrl.engineFile;
        ctrl.enginePakage = StrUtil.getComPackage(ctrl.enginePath);
        ctrl.enginePakage = "com.asiainfo.order.soa.repository.bo";
        dataBean.setEngineClass(ctrl.engineClass);
        dataBean.setEngineFile(ctrl.engineFile);
        dataBean.setEnginePath(ctrl.enginePath);
        dataBean.setEngineFilePath(ctrl.engineFilePath);
        dataBean.setEnginePakage(ctrl.enginePakage);

        // value
        ctrl.valueClass = "I" + ctrl.boName + "Value";
        ctrl.valueFile = ctrl.valueClass + ".java";
        ctrl.valuePath = homePath + "\\ivalues";
        ctrl.valueFilePath = ctrl.valuePath + "\\" + ctrl.valueFile;
        ctrl.valuePackage = StrUtil.getComPackage(ctrl.valuePath);
        ctrl.valuePackage = "com.asiainfo.order.soa.repository.ivalues";
        dataBean.setValueClass(ctrl.valueClass);
        dataBean.setValueFile(ctrl.valueFile);
        dataBean.setValuePath(ctrl.valuePath);
        dataBean.setValueFilePath(ctrl.valueFilePath);
        dataBean.setValuePackage(ctrl.valuePackage);

        // dao
        String name2Package = StrUtil.tableName2Package(ctrl.xlsctrlinfo.tableName);
        ctrl.daoClass = "I" + ctrl.boName + "DAO";
        ctrl.daoFile = ctrl.daoClass + ".java";
        ctrl.daoPath = homePath + "\\dao\\crm\\" + name2Package + "\\interfaces";
        ctrl.daoFilePath = ctrl.daoPath + "\\" + ctrl.daoFile;
        ctrl.daoPackage = "com.asiainfo.order.soa.repository.dao.crm." + name2Package + ".interfaces";
        dataBean.setDaoClass(ctrl.daoClass);
        dataBean.setDaoFile(ctrl.daoFile);
        dataBean.setDaoPath(ctrl.daoPath);
        dataBean.setDaoFilePath(ctrl.daoFilePath);
        dataBean.setDaoPackage(ctrl.daoPackage);

        dataBean.setDaoImplClass(ctrl.boName + "DAOImpl");
        dataBean.setDaoImplFile(dataBean.getDaoImplClass() + ".java");
        dataBean.setDaoImplPath(homePath + "\\dao\\crm\\" + name2Package + "\\impl");
        dataBean.setDaoImplFilePath(dataBean.getDaoImplPath() + "\\" + dataBean.getDaoImplFile());
        dataBean.setDaoImplPackage("com.asiainfo.order.soa.repository.dao.crm." + name2Package + ".impl");
        return dataBean;
    }

    private static void setColumnInfo(List<ColumnInfo> columnList) {

        ColumnInfo ci = new ColumnInfo();
        columnList.add(ci);

        ci.columnName = "CUST_ID";
        ci.columnTypeName = "NUMBER";
        ci.colunmPrecision = 16;
        ci.colunmScale = 0;

        ci = new ColumnInfo();
        columnList.add(ci);

        ci.columnName = "CUST_NAME";
        ci.columnTypeName = "VARCHAR2";
        ci.colunmPrecision = 400;
        ci.colunmScale = 0;

        ci = new ColumnInfo();
        columnList.add(ci);

        ci.columnName = "SEX";
        ci.columnTypeName = "CHAR";
        ci.colunmPrecision = 1;
        ci.colunmScale = 0;

        ci = new ColumnInfo();
        columnList.add(ci);

        ci.columnName = "VALID_DATE";
        ci.columnTypeName = "DATE";
        ci.colunmPrecision = 1;
        ci.colunmScale = 0;

        ci = new ColumnInfo();
        columnList.add(ci);

        ci.columnName = "CUST_TYPE";
        ci.columnTypeName = "NUMBER";
        ci.colunmPrecision = 4;
        ci.colunmScale = 0;
        ci = new ColumnInfo();
        columnList.add(ci);

        ci.columnName = "MONEY_FEE";
        ci.columnTypeName = "NUMBER";
        ci.colunmPrecision = 10;
        ci.colunmScale = 2;
    }

    private static void setDaoFileInfo(BoCtrlInfo ctrl) throws Exception {
        List<String> list;
        String filepath;
        String src = "";
        String func = "";

        // =========================
        filepath = ctrl.xlsctrlinfo.daoQueryItfFilePath;

        //
        if (ctrl.keyCnt > 0) {
            list = getMapList(ctrl, filepath, DaoScType.dao_c1ib);
            src = "import " + ctrl.beanPackage + "" + ctrl.beanClass + ";";
            list.add(src);

            list = getMapList(ctrl, filepath, DaoScType.dao_c1m);
            src = "    public " + ctrl.beanClass + " query" + ctrl.stdName + "(" + ctrl.paramStr + ") throws Exception";
            list.add(src);
            func = src;
        }

        //
        list = getMapList(ctrl, filepath, DaoScType.dao_c1p);
        src = "public interface " + ctrl.xlsctrlinfo.daoQueryItfClass;
        list.clear();
        list.add(src);

        // =========================
        filepath = ctrl.xlsctrlinfo.daoQueryImplFilePath;

        //
        if (ctrl.keyCnt > 0) {
            list = getMapList(ctrl, filepath, DaoScType.dao_c2ib);
            src = "import " + ctrl.beanPackage + "" + ctrl.beanClass + ";";
            list.add(src);

            list = getMapList(ctrl, filepath, DaoScType.dao_c2ie);
            src = "import " + ctrl.enginePakage + "" + ctrl.engineClass + ";";
            list.add(src);

            //
            list = getMapList(ctrl, filepath, DaoScType.dao_c2m);
            src = func + "\r\n    {\r\n        return " + ctrl.engineClass + ".getBean(" + ctrl.paramValue + ");\r\n    " + "}";
            list.add(src);
        }

        list = getMapList(ctrl, filepath, DaoScType.dao_c2ii);
        src = "import " + ctrl.xlsctrlinfo.daoItfPackage + "" + ctrl.xlsctrlinfo.daoQueryItfClass + ";";
        list.clear();
        list.add(src);

        //
        list = getMapList(ctrl, filepath, DaoScType.dao_c2p);
        src = "public class " + ctrl.xlsctrlinfo.daoQueryImplClass + " implements " + ctrl.xlsctrlinfo.daoQueryItfClass;
        list.clear();
        list.add(src);

        // =========================
        filepath = ctrl.xlsctrlinfo.daoOperItfFilePath;

        //
        list = getMapList(ctrl, filepath, DaoScType.dao_c3iv);
        src = "import " + ctrl.valuePackage + "" + ctrl.valueClass + ";";
        list.add(src);

        //
        list = getMapList(ctrl, filepath, DaoScType.dao_c3p);
        src = "public interface " + ctrl.xlsctrlinfo.daoOperItfClass;
        list.clear();
        list.add(src);

        //
        list = getMapList(ctrl, filepath, DaoScType.dao_c3m);
        src = "    public void save" + ctrl.stdName + "(" + ctrl.valueClass + " [] values) throws Exception";
        list.add(src);
        func = src;

        // =========================
        filepath = ctrl.xlsctrlinfo.daoOperImplFilePath;

        //
        list = getMapList(ctrl, filepath, DaoScType.dao_c4iv);
        src = "import " + ctrl.valuePackage + "" + ctrl.valueClass + ";";
        list.add(src);

        list = getMapList(ctrl, filepath, DaoScType.dao_c4ie);
        src = "import " + ctrl.enginePakage + "" + ctrl.engineClass + ";";
        list.add(src);

        list = getMapList(ctrl, filepath, DaoScType.dao_c4ii);
        src = "import " + ctrl.xlsctrlinfo.daoItfPackage + "" + ctrl.xlsctrlinfo.daoOperItfClass + ";";
        list.clear();
        list.add(src);

        //
        list = getMapList(ctrl, filepath, DaoScType.dao_c4p);
        src = "public class " + ctrl.xlsctrlinfo.daoOperImplClass + " implements " + ctrl.xlsctrlinfo.daoOperItfClass;
        list.clear();
        list.add(src);

        //
        list = getMapList(ctrl, filepath, DaoScType.dao_c4m);
        src = func + "\r\n    {\r\n        " + ctrl.engineClass + ".save(values);\r\n    " + "}";
        list.add(src);
    }

    private static void setDaoSvCtrlInfo(BoCtrlInfo ctrl) throws Exception {
        String homePath = ctrl.modelHomePath + "\\" + ctrl.xlsctrlinfo.subpath;

        // ---------------------

        // dao impl
        ctrl.xlsctrlinfo.daoImplClass = ctrl.stdName + "DAOImpl";
        ctrl.xlsctrlinfo.daoImplFile = ctrl.xlsctrlinfo.daoImplClass + ".java";
        ctrl.xlsctrlinfo.daoImplPath = homePath + "\\dao\\impl";

        // dao interfaces
        ctrl.xlsctrlinfo.daoItfClass = "I" + ctrl.stdName + "DAO";
        ctrl.xlsctrlinfo.daoItfFile = ctrl.xlsctrlinfo.daoItfClass + ".java";
        ctrl.xlsctrlinfo.daoItfPath = homePath + "\\dao\\interfaces";

        ctrl.xlsctrlinfo.daoItfPackage = StrUtil.getComPackage(ctrl.xlsctrlinfo.daoItfPath);

        // sv impl
        ctrl.svImplClass = ctrl.stdName + "SVImpl";
        ctrl.svImplFile = ctrl.svImplClass + ".java";

        ctrl.svImplPath = homePath + "\\service\\impl";

        ctrl.svImplPackage = StrUtil.getComPackage(ctrl.svImplPath);

        // sv interfaces
        ctrl.svItfClass = "I" + ctrl.stdName + "SV";
        ctrl.svItfFile = ctrl.svItfClass + ".java";

        ctrl.svItfPath = homePath + "\\service\\interfaces";

        ctrl.xlsctrlinfo.svItfPackage = StrUtil.getComPackage(ctrl.svItfPath);

        // svc
        ctrl.svcClass = "QryXxxByYyySVC";
        ctrl.svcFile = ctrl.svcClass + ".java";

        ctrl.svcPath = ctrl.modelHomePath + "\\svc";

        ctrl.svcFilePath = ctrl.svcPath + "\\" + ctrl.svcFile;
        ctrl.svcPackage = StrUtil.getComPackage(ctrl.svcPath);

        // path
        String path2 = ctrl.xlsctrlinfo.center2 + ctrl.xlsctrlinfo.funmodel2;

        // dao class
        ctrl.xlsctrlinfo.daoOperImplClass = path2 + "OperateDAOImpl";
        ctrl.xlsctrlinfo.daoQueryImplClass = path2 + "QueryDAOImpl";
        ctrl.xlsctrlinfo.daoOperItfClass = "I" + path2 + "OperateDAO";
        ctrl.xlsctrlinfo.daoQueryItfClass = "I" + path2 + "QueryDAO";

        // dao file
        ctrl.xlsctrlinfo.daoOperImplFile = ctrl.xlsctrlinfo.daoOperImplClass + ".java";
        ctrl.xlsctrlinfo.daoQueryImplFile = ctrl.xlsctrlinfo.daoQueryImplClass + ".java";
        ctrl.xlsctrlinfo.daoOperItfFile = ctrl.xlsctrlinfo.daoOperItfClass + ".java";
        ctrl.xlsctrlinfo.daoQueryItfFile = ctrl.xlsctrlinfo.daoQueryItfClass + ".java";

        // sv class
        ctrl.xlsctrlinfo.svOperImplClass = path2 + "OperateSVImpl";
        ctrl.xlsctrlinfo.svQueryImplClass = path2 + "QuerySVImpl";
        ctrl.xlsctrlinfo.svOperItfClass = "I" + path2 + "OperateSV";
        ctrl.xlsctrlinfo.svQueryItfClass = "I" + path2 + "QuerySV";

        // dao file
        ctrl.xlsctrlinfo.svOperImplFile = ctrl.xlsctrlinfo.svOperImplClass + ".java";
        ctrl.xlsctrlinfo.svQueryImplFile = ctrl.xlsctrlinfo.svQueryImplClass + ".java";
        ctrl.xlsctrlinfo.svOperItfFile = ctrl.xlsctrlinfo.svOperItfClass + ".java";
        ctrl.xlsctrlinfo.svQueryItfFile = ctrl.xlsctrlinfo.svQueryItfClass + ".java";

        // path
        ctrl.xlsctrlinfo.daoQueryItfFilePath = ctrl.xlsctrlinfo.daoItfPath + "\\" + ctrl.xlsctrlinfo.daoQueryItfFile;
        ctrl.xlsctrlinfo.daoQueryImplFilePath = ctrl.xlsctrlinfo.daoImplPath + "\\" + ctrl.xlsctrlinfo.daoQueryImplFile;

        ctrl.xlsctrlinfo.daoOperItfFilePath = ctrl.xlsctrlinfo.daoItfPath + "\\" + ctrl.xlsctrlinfo.daoOperItfFile;
        ctrl.xlsctrlinfo.daoOperImplFilePath = ctrl.xlsctrlinfo.daoImplPath + "\\" + ctrl.xlsctrlinfo.daoOperImplFile;

        ctrl.xlsctrlinfo.svQueryItfFilePath = ctrl.svItfPath + "\\" + ctrl.xlsctrlinfo.svQueryItfFile;
        ctrl.xlsctrlinfo.svQueryImplFilePath = ctrl.svImplPath + "\\" + ctrl.xlsctrlinfo.svQueryImplFile;

        ctrl.xlsctrlinfo.svOperItfFilePath = ctrl.svItfPath + "\\" + ctrl.xlsctrlinfo.svOperItfFile;
        ctrl.xlsctrlinfo.svOperImplFilePath = ctrl.svImplPath + "\\" + ctrl.xlsctrlinfo.svOperImplFile;
    }

    private static void setSvFileInfo(BoCtrlInfo ctrl) throws Exception {
        List<String> list;
        String filepath;
        String src = "";
        String func = "";

        // =========================
        filepath = ctrl.xlsctrlinfo.svQueryItfFilePath;

        //
        if (ctrl.keyCnt > 0) {
            list = getMapList(ctrl, filepath, DaoScType.sv_c1ib);
            src = "import " + ctrl.beanPackage + "" + ctrl.beanClass + ";";
            list.add(src);

            list = getMapList(ctrl, filepath, DaoScType.sv_c1m);
            src = "    public " + ctrl.beanClass + " query" + ctrl.stdName + "(" + ctrl.paramStr + ") throws Exception";
            list.add(src);
            func = src;
        }

        //
        list = getMapList(ctrl, filepath, DaoScType.sv_c1p);
        src = "public interface " + ctrl.xlsctrlinfo.svQueryItfClass;
        list.clear();
        list.add(src);

        // =========================
        filepath = ctrl.xlsctrlinfo.svQueryImplFilePath;

        if (ctrl.keyCnt > 0) {
            //
            list = getMapList(ctrl, filepath, DaoScType.sv_c2ib);
            src = "import " + ctrl.beanPackage + "" + ctrl.beanClass + ";";
            list.add(src);

            list = getMapList(ctrl, filepath, DaoScType.sv_c2id);
            src = "import " + ctrl.xlsctrlinfo.daoItfPackage + "" + ctrl.xlsctrlinfo.daoQueryItfClass + ";";
            list.clear();
            list.add(src);

            //
            list = getMapList(ctrl, filepath, DaoScType.sv_c2m);
            src = func + "\r\n    {\r\n        " + ctrl.xlsctrlinfo.daoQueryItfClass + " dao = (" + ctrl.xlsctrlinfo.daoQueryItfClass + ") ServiceFactory.getService(" + ctrl.xlsctrlinfo.daoQueryItfClass + ".class);\r\n\r\n        return dao.query"
                    + ctrl.stdName + "(" + ctrl.paramValue + ");\r\n    " + "}";
            list.add(src);
        }

        list = getMapList(ctrl, filepath, DaoScType.sv_c2ii);
        src = "import " + ctrl.xlsctrlinfo.svItfPackage + "" + ctrl.xlsctrlinfo.svQueryItfClass + ";";
        list.clear();
        list.add(src);

        //
        list = getMapList(ctrl, filepath, DaoScType.sv_c2p);
        src = "public class " + ctrl.xlsctrlinfo.svQueryImplClass + " implements " + ctrl.xlsctrlinfo.svQueryItfClass;
        list.clear();
        list.add(src);

        // =========================
        filepath = ctrl.xlsctrlinfo.svOperItfFilePath;

        //
        list = getMapList(ctrl, filepath, DaoScType.sv_c3iv);
        src = "import " + ctrl.valuePackage + "" + ctrl.valueClass + ";";
        list.add(src);

        //
        list = getMapList(ctrl, filepath, DaoScType.sv_c3p);
        src = "public interface " + ctrl.xlsctrlinfo.svOperItfClass;
        list.clear();
        list.add(src);

        //
        list = getMapList(ctrl, filepath, DaoScType.sv_c3m);
        src = "    public void save" + ctrl.stdName + "(List<" + ctrl.valueClass + "> values) throws Exception";
        list.add(src);
        func = src;

        // =========================
        filepath = ctrl.xlsctrlinfo.svOperImplFilePath;

        //
        list = getMapList(ctrl, filepath, DaoScType.sv_c4iv);
        src = "import " + ctrl.valuePackage + "" + ctrl.valueClass + ";";
        list.add(src);

        list = getMapList(ctrl, filepath, DaoScType.sv_c4id);
        src = "import " + ctrl.xlsctrlinfo.daoItfPackage + "" + ctrl.xlsctrlinfo.daoOperItfClass + ";";
        list.clear();
        list.add(src);

        list = getMapList(ctrl, filepath, DaoScType.sv_c4ii);
        src = "import " + ctrl.xlsctrlinfo.svItfPackage + "" + ctrl.xlsctrlinfo.svOperItfClass + ";";
        list.clear();
        list.add(src);

        //
        list = getMapList(ctrl, filepath, DaoScType.sv_c4p);
        src = "public class " + ctrl.xlsctrlinfo.svOperImplClass + " implements " + ctrl.xlsctrlinfo.svOperItfClass;
        list.clear();
        list.add(src);

        //
        list = getMapList(ctrl, filepath, DaoScType.sv_c4m);
        src = func + "\r\n    {\r\n        " + ctrl.xlsctrlinfo.daoOperItfClass + " dao = (" + ctrl.xlsctrlinfo.daoOperItfClass + ") ServiceFactory.getService(" + ctrl.xlsctrlinfo.daoOperItfClass + ".class);\r\n\r\n        " + ctrl.valueClass
                + "[] datas = values.toArray(new " + ctrl.valueClass + "[] {});\r\n\r\n        dao.save" + ctrl.stdName + "(datas);\r\n    }";
        list.add(src);
    }

    private static void showColumnInfo(BoCtrlInfo ctrl, List<ColumnInfo> columnList) throws Exception {
        if (ctrl.isShow == false) {
            return;
        }

        int i = 0;

        for (ColumnInfo ci : columnList) {
            i++;

            SysLog.log(ctrl.logName, "======================================[" + i + "]");
            SysLog.log(ctrl.logName, "tableName:[" + i + "]" + ci.tableName);
            SysLog.log(ctrl.logName, "columnName:[" + i + "]" + ci.columnName);
            SysLog.log(ctrl.logName, "columnRemark:[" + i + "]" + ci.columnRemark);
            SysLog.log(ctrl.logName, "columnTypeName:[" + i + "]" + ci.columnTypeName);
            SysLog.log(ctrl.logName, "colunmPrecision:[" + i + "]" + ci.colunmPrecision);
            SysLog.log(ctrl.logName, "colunmScale:[" + i + "]" + ci.colunmScale);
            SysLog.log(ctrl.logName, "varName:[" + i + "]" + ci.varName);
            SysLog.log(ctrl.logName, "boDataType:[" + i + "]" + ci.boDataType);
        }
    }
}
