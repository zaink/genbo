package com.ai.luoying.genbofile;

public class XlsCtrlInfo
{
    public boolean ispart = false;

    public String tableName;

    public String center;

    public String center2;

    public String funmodel;

    public String funmodel2;

    public String subpath;

    // dao class
    public String daoQueryItfClass;

    public String daoQueryImplClass;

    public String daoOperItfClass;

    public String daoOperImplClass;

    // dao file
    public String daoQueryItfFile;

    public String daoQueryImplFile;

    public String daoOperItfFile;

    public String daoOperImplFile;

    // sv class
    public String svQueryItfClass;

    public String svOperItfClass;

    public String svQueryImplClass;

    public String svOperImplClass;

    // dao file
    public String svQueryItfFile;

    public String svQueryImplFile;

    public String svOperItfFile;

    public String svOperImplFile;

    //
    public String daoQueryItfFilePath;

    public String daoQueryImplFilePath;

    public String daoOperItfFilePath;

    public String daoOperImplFilePath;

    //

    public String svQueryItfFilePath;

    public String svQueryImplFilePath;

    public String svOperItfFilePath;

    public String svOperImplFilePath;

    //
    public String daoImplClass;

    public String daoImplPath;

    public String daoImplFile;

    //
    public String daoItfClass;

    public String daoItfPath;

    public String daoItfFile;

    //
    public String daoItfPackage;

    public String svItfPackage;
}
