package com.ai.luoying.genbofile;

public class ColumnInfo {
    public boolean keyIs;

    public int keySeq;

    public String tableName;

    public String columnName;

    public int columnType;

    public String columnTypeName;

    public String columnRemark;

    public int colunmPrecision;

    public int colunmScale;

    public String firstUpperName;

    public String firstLowerName;

    public String varName;

    public String boDataType;

    public String boDataType2;

    public boolean idxIs;

}
