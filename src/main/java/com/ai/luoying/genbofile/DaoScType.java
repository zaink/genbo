package com.ai.luoying.genbofile;

public class DaoScType
{
    // daoQueryItfFilePath
    public static final String dao_c1ib = "dao_c1ib";

    public static final String dao_c1p = "dao_c1p";

    public static final String dao_c1m = "dao_c1m";

    // daoQueryImplFilePath
    public static final String dao_c2ib = "dao_c2ib";

    public static final String dao_c2ie = "dao_c2ie";

    public static final String dao_c2ii = "dao_c2ii";

    public static final String dao_c2p = "dao_c2p";

    public static final String dao_c2m = "dao_c2m";

    // daoOperItfFilePath
    public static final String dao_c3iv = "dao_c3iv";

    public static final String dao_c3p = "dao_c3p";

    public static final String dao_c3m = "dao_c3m";

    // daoOperImplFilePath
    public static final String dao_c4iv = "dao_c4iv";

    public static final String dao_c4ie = "dao_c4ie";

    public static final String dao_c4ii = "dao_c4ii";

    public static final String dao_c4p = "dao_c4p";

    public static final String dao_c4m = "dao_c4m";

    // svOperItfFilePath
    public static final String sv_c1ib = "sv_c1ib";

    public static final String sv_c1p = "sv_c1p";

    public static final String sv_c1m = "sv_c1m";

    // svQueryImplFilePath
    public static final String sv_c2ib = "sv_c2ib";

    public static final String sv_c2id = "sv_c2id";

    public static final String sv_c2ii = "sv_c2ii";

    public static final String sv_c2p = "sv_c2p";

    public static final String sv_c2m = "sv_c2m";

    // svOperItfFilePath
    public static final String sv_c3iv = "sv_c3iv";

    public static final String sv_c3p = "sv_c3p";

    public static final String sv_c3m = "sv_c3m";

    // svOperImplFilePath
    public static final String sv_c4iv = "sv_c4iv";

    public static final String sv_c4id = "sv_c4id";

    public static final String sv_c4ii = "sv_c4ii";

    public static final String sv_c4p = "sv_c4p";

    public static final String sv_c4m = "sv_c4m";
}
