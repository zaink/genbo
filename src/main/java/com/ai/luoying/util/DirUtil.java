package com.ai.luoying.util;

import java.io.File;

public class DirUtil
{

    public static boolean mkDirs(String pathname) throws Exception
    {
        File myPath = new File(pathname);
        return myPath.mkdirs();
    }

}
