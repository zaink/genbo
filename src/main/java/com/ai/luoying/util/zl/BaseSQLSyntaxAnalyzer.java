package com.ai.luoying.util.zl;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import org.apache.commons.lang.StringUtils;

import com.ai.luoying.util.zl.interfaces.ISQLSyntaxAnalyzer;

/**
 * Copyright: Copyright (c) 2013 Asiainfo-Linkage
 *
 * @className: BaseSQLSyntaxAnalyzer
 * @description: SQL语法分析器基类 todo list: 1. 注释的影响。 2. WITH TABLE_NAME AS语句的影响。 WITH T AS (SELECT * FROM TD_S_STATIC WHERE TYPE_ID = 'TD_S_ELEMENTTYPE') SELECT * FROM T WHERE DATA_ID = 'D';
 * @version: v1.0.0
 * @author: zhoulin2
 * @date: 2013-3-4
 */
public class BaseSQLSyntaxAnalyzer implements ISQLSyntaxAnalyzer
{

    // 参数前缀
    private static final String PREFIX = ":";

    // 表名占位符
    private static final String TAB_PLACEHOLDER = "@TABLE@";

    /**
     * 判断是否为合法的参数字符 0-9,A-Z,a-z,_
     *
     * @param c
     * @return
     */
    private static boolean isVariableChar(char c)
    {
        if (48 <= c && c <= 57)
        {
            return true; // 0-9
        }
        if (65 <= c && c <= 90)
        {
            return true; // A-Z
        }
        if (97 <= c && c <= 122)
        {
            return true; // a-z
        }
        if (95 == c)
        {
            return true; // _
        }

        return false;
    }

    // FROM后的所有关键字
    private Set<String> keywords = null;

    /**
     * 从指定位置往后抽取FROM语句片段
     *
     * @param sqlTemplate
     * @param fromIndex
     * @return
     */
    private String extractFromString(String sqlTemplate, int fromIndex)
    {

        StringBuilder buff = new StringBuilder();

        int i = fromIndex;
        List<Integer> indexes = keywordIndexes(sqlTemplate);

        for (int keyPosition : indexes)
        {
            while (i < keyPosition)
            {
                char c = sqlTemplate.charAt(i);

                if ('(' == c)
                {
                    i = findRightBracket(sqlTemplate, i);
                    if (-1 == i)
                    {
                        throw new RuntimeException("SQL语法错误，无法解析！");
                    }
                    buff.append(TAB_PLACEHOLDER); // 子查询占位符
                }
                else
                {
                    buff.append(c);
                }

                i++;
            }

            if (i == keyPosition)
            {
                return buff.toString();
            }
        }

        if (buff.length() > 0)
        {
            return buff.toString();
        }
        else
        {
            return sqlTemplate.substring(fromIndex);
        }
    }

    /**
     * 在beginIndex和endIndex之间抽取参数名，找到第一个非变量字符即停止。
     *
     * @param begIndex
     * @param endIndex
     * @return
     */
    private String extractParameter(String str, int begIndex, int endIndex)
    {
        begIndex += 1;
        for (int i = begIndex; i < endIndex; i++)
        {
            if (isVariableChar(str.charAt(i)))
            {
                continue;
            }
            return str.substring(begIndex, i);
        }

        return str.substring(begIndex, endIndex);
    }

    /**
     * 从SQL模板中抽取所有变量(以:开头)，以集合形式返回
     *
     * @param sqlTemplate
     * @return
     */
    public Set<String> extractParameters(String sqlTemplate) throws Exception
    {

        Set<String> rtn = new HashSet<String>();

        int begIndex, endIndex;
        List<Integer> indexes = findIndexes(sqlTemplate, PREFIX);
        if (0 == indexes.size())
        {
            return rtn;
        }

        if (1 == indexes.size())
        {
            begIndex = indexes.get(0);
            endIndex = sqlTemplate.length();

            String paramName = extractParameter(sqlTemplate, begIndex, endIndex);
            rtn.add(paramName);
            return rtn;
        }

        for (int i = 0, size = indexes.size(); i < size; i++)
        {
            begIndex = indexes.get(i);

            if (i == size - 1)
            {
                endIndex = sqlTemplate.length();
            }
            else
            {
                endIndex = indexes.get(i + 1);
            }

            String paramName = extractParameter(sqlTemplate, begIndex, endIndex);
            rtn.add(paramName);

        }

        return rtn;
    }

    /**
     * 从SQL模板中抽取所有表名，以集合形式返回
     *
     * @param sqlTemplate
     * @return
     */
    public Set<String> extractTables(String sqlTemplate) throws Exception
    {

        Set<String> rtn = new HashSet<String>();

        // 对语句进行预修饰
        sqlTemplate = sqlTemplate.toUpperCase();
        sqlTemplate = StringUtils.replaceChars(sqlTemplate, "\n\r", " ");
        sqlTemplate = StringUtils.replaceChars(sqlTemplate, "\n", " ");
        sqlTemplate = StringUtils.replaceChars(sqlTemplate, "\t", " ");

        List<Integer> indexes = findIndexes(sqlTemplate, " FROM ");

        for (int i : indexes)
        {

            String fromString = extractFromString(sqlTemplate, i);
            if (null == fromString)
            {
                continue;
            }

            fromString = fromString.trim().substring(5);
            String[] tables = StringUtils.split(fromString, ",");
            for (String table : tables)
            {
                String[] slices = StringUtils.split(table, ' ');
                if (slices[0].startsWith(TAB_PLACEHOLDER))
                {
                    continue;
                }

                // 去SCHEMA前缀
                String[] tmp = StringUtils.split(slices[0], '.');
                rtn.add(tmp[tmp.length - 1]);
            }
        }

        return rtn;
    }

    /**
     * 返回匹配needle的所有位置集合
     *
     * @param sqlTemplate
     * @param needle
     * @return
     */
    private List<Integer> findIndexes(String haystack, String needle)
    {
        List<Integer> rtn = new LinkedList<Integer>();

        boolean inQuota = false;
        for (int i = 0, len = haystack.length(); i < len; i++)
        {
            char c = haystack.charAt(i);

            if ('\'' == c)
            {
                inQuota = inQuota ? false : true; // 将状态反置
            }

            if (inQuota)
            { // 在引号中
                continue;
            }
            else
            {
                if (haystack.startsWith(needle, i))
                {
                    rtn.add(i);
                }
            }
        }

        return rtn;
    }

    /**
     * 从指定位置往后找与之匹配的右括号
     *
     * @param s
     * @param fromIndex
     * @return
     */
    private int findRightBracket(String str, int fromIndex)
    {

        Stack<Character> stack = new Stack<Character>();

        for (int i = fromIndex, len = str.length(); i < len; i++)
        {
            char c = str.charAt(i);
            if ('(' == c)
            {
                stack.push(c);
            }
            else if (')' == c)
            {
                stack.pop(); // 弹出与之匹配的左括号
                if (0 == stack.size())
                { // 当栈为空时，便找到了
                    return i;
                }
            }
        }

        return -1;
    }

    /**
     * 返回SQL模板中所有符合（WHERE尾关键字）的位置
     *
     * @param sqlTemplate
     * @return
     */
    private List<Integer> keywordIndexes(String sqlTemplate)
    {

        List<Integer> indexes = new LinkedList<Integer>();

        for (String keyword : keywords)
        {
            indexes.addAll(findIndexes(sqlTemplate, keyword));
        }

        Collections.sort(indexes);
        return indexes;
    }

    public void setKeywords(Set<String> keywords)
    {
        this.keywords = keywords;
    }

}
