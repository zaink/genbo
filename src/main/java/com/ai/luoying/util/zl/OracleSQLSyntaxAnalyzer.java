package com.ai.luoying.util.zl;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashSet;
import java.util.Set;

import com.ai.luoying.util.zl.interfaces.ISQLSyntaxAnalyzer;

/**
 * Copyright: Copyright (c) 2013 Asiainfo-Linkage
 *
 * @className: OracleSQLSyntaxAnalyzer
 * @description: ORACLE SQL语法分析器
 * @version: v1.0.0
 * @author: zhoulin2
 * @date: 2013-3-4
 */
public class OracleSQLSyntaxAnalyzer extends BaseSQLSyntaxAnalyzer
{

    private static final OracleSQLSyntaxAnalyzer instance = new OracleSQLSyntaxAnalyzer();

    public static ISQLSyntaxAnalyzer getInstance()
    {
        return instance;
    }

    public static void main(String[] args)
    {
        String sql = null;
        try
        {
            File file = new File("C:/test.sql");
            FileInputStream is = new FileInputStream(file);
            int size = (int) file.length();
            byte[] bytes = new byte[size];
            is.read(bytes);

            sql = new String(bytes);
            Set<String> tables = instance.extractTables(sql);
            System.out.println(tables);

            Set<String> parameters = instance.extractParameters(sql);
            System.out.println(parameters);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private OracleSQLSyntaxAnalyzer()
    {

        Set<String> keywords = new HashSet<String>();

        keywords.add(" WHERE "); // WHERE
        keywords.add(" GROUP "); // GROUP BY
        keywords.add(" HAVING "); // HAVING
        keywords.add(" ORDER "); // ORDER BY
        keywords.add(" INNER "); // INNER JOIN
        keywords.add(" LEFT "); // LEFT JOIN
        keywords.add(" RIGHT "); // RIGHT JOIN
        keywords.add(" START "); // START WITH
        keywords.add(")"); // RIGHT BRACKET

        super.setKeywords(keywords);

    }
}
