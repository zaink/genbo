package com.ai.luoying.util.zl;

public class TrimSQLComment
{

    public static void main(String[] args)
    {
        System.out.println(trimSqlComment("-- select * from td_s_static"));
        System.out.println(trimSqlComment("select '--' from td_s_static"));
        System.out.println(trimSqlComment("select * from td_s_static -- xxxxxx"));
        System.out.println(trimSqlComment("select * from td_s_static where id='--XX'"));
        System.out.println(trimSqlComment("select * from td_s_static where id='--XX' -- XXXXXX"));
        System.out.println(trimSqlComment("select * from td_s_static where id='2' --XXX'X'XX"));
        System.out.println(trimSqlComment("where id=123 --XXX'X'XX"));

    }

    /**
     * 抹掉ORACLE SQL注释
     *
     * @param rawSql
     * @return
     */
    public static final StringBuilder trimSqlComment(String rawSql)
    {

        StringBuilder sql = new StringBuilder(4000);

        boolean inQuota = false;
        for (int i = 0, len = rawSql.length(); i < len; i++)
        {
            char c = rawSql.charAt(i);

            if ('\'' == c)
            {
                inQuota = inQuota ? false : true; // 将状态反置
            }

            if (inQuota)
            {
                // 在引号中
                sql.append(c);
            }
            else
            {
                if ('-' == c && '-' == rawSql.charAt(i + 1))
                {
                    return sql;
                }
                else
                {
                    sql.append(c);
                }
            }
        }

        return sql;
    }
}
