package com.ai.luoying.util.zl.interfaces;

import java.util.Set;

/**
 * Copyright: Copyright (c) 2013 Asiainfo-Linkage
 *
 * @className: ISQLSyntaxAnalyzer
 * @description: SQL语法分析接口
 * @version: v1.0.0
 * @author: zhoulin2
 * @date: 2013-3-4
 */
public interface ISQLSyntaxAnalyzer
{

    /**
     * 从SQL模板中抽取所有变量(以:开头)，以集合形式返回
     *
     * @param sqlTemplate
     * @return
     */
    public Set<String> extractParameters(String sqlTemplate) throws Exception;

    /**
     * 从SQL模板中抽取所有表名，以集合形式返回
     *
     * @param sqlTemplate
     * @return
     */
    public Set<String> extractTables(String sqlTemplate) throws Exception;
}
