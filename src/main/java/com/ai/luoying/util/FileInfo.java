package com.ai.luoying.util;

public class FileInfo
{
    public String filePath;

    public String fileName;

    public String fileNameNoSuffix;

    public String filePathName;

    public String fileType;

    public String filePackage;
}
