package com.ai.luoying.util;

import org.apache.commons.lang.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StrUtil {
    // 文件编码格式
    public final static String encoding = "UTF-8";

    public static String replaceBlank(String str) {
        String dest = "";
        if (str != null) {
            Pattern p = Pattern.compile("\\s*|\t|\r|\n");
            Matcher m = p.matcher(str);
            dest = m.replaceAll("");
        }
        return dest;
    }

    public static String addstr(String src, String desc) throws Exception {
        if (StringUtils.isBlank(src)) {
            src = desc;
        } else {
            if (src.indexOf(desc) == -1) {
                src = src + "\r\n" + desc;
            }
        }

        return src;
    }

    public static String firstLowerName(String name) {
        String firstLetter = "";
        String restLetters = "";
        String result = "";

        firstLetter = name.substring(0, 1).toLowerCase();
        restLetters = name.substring(1);

        result = result + firstLetter + restLetters;

        return result;
    }

    public static String firstUpperName(String name) {
        String[] s = name.split("_");

        String firstLetter = "";
        String restLetters = "";
        String result = "";

        String data = "";
        for (String element : s) {
            data = element;

            firstLetter = data.substring(0, 1).toUpperCase();
            restLetters = data.substring(1).toLowerCase();

            result = result + firstLetter + restLetters;
        }

        return result;
    }

    public static String getFilePath(String filePath) {
        int index = filePath.lastIndexOf("\\");

        String path = filePath.substring(0, index);

        return path;
    }

    public static String getComPackage(String path) {
        path = path.replace("\\", "");

        int idx = path.indexOf(".com.");

        path = path.substring(idx + 1);

        path = "";

        return path;
    }

    public static String tableName2Package(String tableName) {
        tableName = tableName.replace("_", "");
        return tableName.toLowerCase();
    }

    public static String getFormat20Str(String str) throws Exception {
        return String.format("%-20s", str);
    }

    public static String getFormat12Str(String str) throws Exception {
        return String.format("%-12s", str);
    }
}
