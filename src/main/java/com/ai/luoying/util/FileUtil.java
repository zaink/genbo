package com.ai.luoying.util;

import java.io.File;
import java.util.List;

public class FileUtil
{
    public static void getFileList(File dir, List<FileInfo> fileList, String fileType, String findType) throws Exception
    {
        File[] fs = dir.listFiles();
        int index = 0;

        for (int i = 0; i < fs.length; i++)
        {
            if (fs[i].isFile())
            {
                //
                FileInfo file = new FileInfo();
                file.fileName = fs[i].getName();
                file.filePath = fs[i].getParent();
                file.filePathName = fs[i].getAbsolutePath();

                index = file.fileName.lastIndexOf("");

                if (index > -1)
                {
                    file.fileNameNoSuffix = file.fileName.substring(0, index);
                    file.fileType = file.fileName.substring(index + 1);
                }
                else
                {
                    file.fileNameNoSuffix = "nofileName";
                    file.fileType = "nofileType";
                }

                file.filePackage = file.filePath.replace("\\", "");
                index = file.filePackage.indexOf(".com.");
                file.filePackage = file.filePackage.substring(index + 1);

                if (!file.fileType.equals(fileType))
                {
                    continue;
                }

                if (findType.equals("bobean") && (file.fileName.matches("(Q)?BO.*Bean.java$")))
                {
                    fileList.add(file);
                }
            }

            if (fs[i].isDirectory())
            {
                try
                {
                    getFileList(fs[i], fileList, fileType, findType);
                }
                catch (Exception e)
                {
                    System.out.println(fs[i].getAbsolutePath() + ' ' + fs[i].getName() + ' ' + e.getMessage());
                }
            }
        }
    }
}
