package com.ai.luoying.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;

public class IniReader
{
    protected HashMap<String, Properties> sections = new HashMap<String, Properties>();

    protected HashMap<String, List<String>> sectionsList = new HashMap<String, List<String>>();

    private boolean isSecList = false;

    private transient String currentSecion;

    private transient Properties current;

    private transient List<String> currentList;

    public IniReader(String filename) throws IOException
    {
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        read(reader);
        reader.close();
    }

    public List<String> getSectionKey(String section)
    {
        Properties p = sections.get(section);

        if (p == null)
        {
            return null;
        }

        List<String> keyList = new ArrayList<String>();

        // System.out.println(p.keySet().toString());

        Iterator<Object> it = p.keySet().iterator();
        while (it.hasNext())
        {
            String key = (String) it.next();

            keyList.add(0, key);

            // System.out.println(key);
        }

        // System.out.println(keyList.toString());
        return keyList;
    }

    public String getValue(String section, String name)
    {
        return getValue(section, name, null);
    }

    public String getValue(String section, String name, String defaultValue)
    {
        if (StringUtils.isBlank(name))
        {
            return defaultValue;
        }

        Properties p = sections.get(section);

        if (p == null)
        {
            return defaultValue;
        }

        String value = p.getProperty(name, defaultValue);
        return value;
    }

    public boolean getValueBool(String section, String name, boolean defaultValue)
    {
        String value = getValue(section, name, null);

        if (value == null)
        {
            return defaultValue;
        }

        return Boolean.valueOf(value);
    }

    public List<String> getValueList(String section)
    {
        List<String> p = sectionsList.get(section);

        return p;
    }

    protected void parseLine(String line)
    {
        String myLine = line;

        line = line.trim();

        if (line.matches("\\[.*\\]"))
        {
            isSecList = false;

            currentSecion = line.replaceFirst("\\[(.*)\\]", "$1");
            current = new Properties();
            sections.put(currentSecion, current);
        }
        else if (line.matches("\\{.*\\}"))
        {
            isSecList = true;

            currentSecion = line.replaceFirst("\\{(.*)\\}", "$1");
            currentList = new ArrayList<String>();
            sectionsList.put(currentSecion, currentList);
        }
        else if (line.matches(".*=.*") && isSecList == false)
        {
            if (current != null)
            {
                int i = line.indexOf('=');
                String name = line.substring(0, i).trim();
                String value = line.substring(i + 1).trim();
                current.setProperty(name, value);
            }
        }
        else if (line != null && line.length() > 0 && isSecList == true)
        {
            if (currentList != null)
            {
                currentList.add(myLine);
            }
        }
    }

    protected void read(BufferedReader reader) throws IOException
    {
        String line;
        while ((line = reader.readLine()) != null)
        {
            parseLine(line);
        }
    }
}
