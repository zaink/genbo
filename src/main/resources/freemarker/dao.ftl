package ${daoPackage!};

import com.ai.appframe2.bo.DataContainer;
import ${beanPackage!}.${beanClass!};
import java.util.*;

public interface ${daoClass!}
{

    int saveInstance(${beanClass!} dc) throws Exception ;

    int batchSaveInstance(${beanClass!}[] dcs) throws Exception ;

<#list columnInfos as columnInfo>
<#if columnInfo.keyIs>
    DataContainer getByPk${columnInfo.firstUpperName}(${columnInfo.boDataType} ${columnInfo.firstLowerName} <#if routeFlag>,String regionId</#if>) throws Exception;
</#if>

<#if columnInfo.idxIs && !columnInfo.keyIs>
    DataContainer[] listBy${columnInfo.firstUpperName}(${columnInfo.boDataType} ${columnInfo.firstLowerName}<#if routeFlag>,String regionId</#if>) throws Exception;
</#if>
</#list>
}