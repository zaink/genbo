package ${enginePakage!};

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import ${valuePackage!}.${valueClass!};
import com.ai.appframe2.bo.DataContainerFactory;
import com.ai.appframe2.common.AIException;
import com.ai.appframe2.common.DataContainerInterface;
import com.ai.appframe2.common.ServiceManager;
import com.ai.appframe2.util.criteria.Criteria;

public class ${engineClass!}
{
	/* appframe版本6.0.1新增方法，保存数据时可返回保存数量 */
	public static int save2(${beanClass!} aBean) throws Exception
	{
		Connection conn = null;
		int count = 0;
		try {
			conn = ServiceManager.getSession().getConnection();
			count = ServiceManager.getDataStore().save2(conn,aBean);
		}catch(Exception e){
			throw e;
		}finally{
			if (conn != null)
			conn.close();
		}
		return count;
	}

	public static int save2(${beanClass!}[] aBeans) throws Exception{
		Connection conn = null;
		int count = 0;
		try {
			conn = ServiceManager.getSession().getConnection();
			count = ServiceManager.getDataStore().save2(conn,aBeans);
		}catch(Exception e){
			throw e;
		}finally{
			if (conn != null)
			conn.close();
		}
		return count;
	}

	public static int saveBatch2(${beanClass!}[] aBeans) throws Exception{
		Connection conn = null;
		int count = 0;
		try {
			conn = ServiceManager.getSession().getConnection();
			count = ServiceManager.getDataStore().saveBatch2(conn,aBeans);
		}catch(Exception e){
			throw e;
		}finally{
			if (conn != null)
			conn.close();
		}
		return count;
	}

	public static int save2(${valueClass!} aValue) throws Exception
	{
		return save2(transfer(aValue));
	}

	public static int save2(${valueClass!}[] aValues) throws Exception{
		return save2(transfer(aValues));
	}

	public static int saveBatch2(${valueClass!}[] aValues) throws Exception{
		return saveBatch2(transfer(aValues));
	}

	public static ${beanClass!}[] getBeans(DataContainerInterface dc) throws Exception
	{
		Map ps = dc.getProperties();
		StringBuilder builder = new StringBuilder();
		Map pList = new HashMap();
		for (java.util.Iterator cc = ps.entrySet().iterator(); cc.hasNext();)
		{
			Map.Entry e = (Map.Entry) cc.next();
			if (builder.length() > 0)
			{
				builder.append(" and ");
			}
			builder.append(e.getKey().toString() + " = :p_" + e.getKey().toString());
			pList.put("p_" + e.getKey().toString(), e.getValue());
		}
		Connection conn = ServiceManager.getSession().getConnection();
		try
		{
			return getBeans(builder.toString(), pList);
		}
		finally
		{
			if (conn != null)
			{
			conn.close();
			}
		}
	}

	<#if keyList??>
	public static ${beanClass!} getBean(${paramStr!}) throws Exception
	{
		/** new create */
		String condition = "${condStr!}";
		Map map = new HashMap();
		<#list keyList as keyMap>
		map.put("${keyMap.key}", ${keyMap.value});
		</#list>

		${beanClass!}[] beans = getBeans(condition, map);
		if (beans != null && beans.length == 1)
		{
			return beans[0];
		}
		else if (beans != null && beans.length > 1)
		{
			// [错误]根据主键查询出现一条以上记录
			throw new Exception("[ERROR]More datas than one queryed by PK");
		}
		else
		{
			return null;
		}
	}
	</#if>

	public static ${beanClass!}[] getBeans(Criteria sql) throws Exception
	{
		return getBeans(sql, -1, -1, false);
	}

	public static ${beanClass!}[] getBeans(Criteria sql, int startNum, int endNum, boolean isShowFK) throws Exception
	{
		String[] cols = null;
		String condition = "";
		Map param = null;
		if (sql != null)
		{
			cols = (String[]) sql.getSelectColumns().toArray(new String[0]);
			condition = sql.toString();
			param = sql.getParameters();
		}
		return getBeans(cols, condition, param, startNum, endNum, isShowFK);
	}

	public static ${beanClass!}[] getBeans(String condition, Map parameter) throws Exception
	{
		return getBeans(null, condition, parameter, -1, -1, false);
	}

	public static ${beanClass!}[] getBeans(String[] cols, String condition, Map parameter, int startNum, int endNum, boolean isShowFK) throws Exception
	{
		Connection conn = null;
		try
		{
			conn = ServiceManager.getSession().getConnection();
			return (${beanClass!}[]) ServiceManager.getDataStore().retrieve(conn, ${beanClass!}.class, ${beanClass!}.getObjectTypeStatic(), cols, condition, parameter, startNum, endNum, isShowFK, false, null);
		}
		catch (Exception e)
		{
			throw e;
		}
		finally
		{
			if (conn != null)
			{
				conn.close();
			}
		}
	}

	public static ${beanClass!}[] getBeans(String[] cols, String condition, Map parameter, int startNum, int endNum, boolean isShowFK, String[] extendBOAttrs) throws Exception
	{
		Connection conn = null;
		try
		{
			conn = ServiceManager.getSession().getConnection();
			return (${beanClass!}[]) ServiceManager.getDataStore().retrieve(conn, ${beanClass!}.class, ${beanClass!}.getObjectTypeStatic(), cols, condition, parameter, startNum, endNum, isShowFK, false, extendBOAttrs);
		}
		catch (Exception e)
		{
			throw e;
		}
		finally
		{
			if (conn != null)
			{
				conn.close();
			}
		}
	}

	public static int getBeansCount(String condition, Map parameter) throws Exception
	{
		Connection conn = null;
		try
		{
			conn = ServiceManager.getSession().getConnection();
			return ServiceManager.getDataStore().retrieveCount(conn, ${beanClass!}.getObjectTypeStatic(), condition, parameter, null);
		}
		catch (Exception e)
		{
			throw e;
		}
		finally
		{
			if (conn != null)
			{
				conn.close();
			}
		}
	}

	public static int getBeansCount(String condition, Map parameter, String[] extendBOAttrs) throws Exception
	{
		Connection conn = null;
		try
		{
			conn = ServiceManager.getSession().getConnection();
			return ServiceManager.getDataStore().retrieveCount(conn, ${beanClass!}.getObjectTypeStatic(), condition, parameter, extendBOAttrs);
		}
		catch (Exception e)
		{
			throw e;
		}
		finally
		{
			if (conn != null)
			{
				conn.close();
			}
		}
	}

	public static void save(${beanClass!} aBean) throws Exception
	{
		Connection conn = null;
		try
		{
			conn = ServiceManager.getSession().getConnection();
			ServiceManager.getDataStore().save(conn, aBean);
		}
		catch (Exception e)
		{
			throw e;
		}
		finally
		{
			if (conn != null)
			{
				conn.close();
			}
		}
	}

	public static void save(${beanClass!}[] aBeans) throws Exception
	{
		Connection conn = null;
		try
		{
			conn = ServiceManager.getSession().getConnection();
			ServiceManager.getDataStore().save(conn, aBeans);
		}
		catch (Exception e)
		{
			throw e;
		}
		finally
		{
			if (conn != null)
			{
				conn.close();
			}
		}
	}

	public static void saveBatch(${beanClass!}[] aBeans) throws Exception
	{
		Connection conn = null;
		try
		{
			conn = ServiceManager.getSession().getConnection();
			ServiceManager.getDataStore().saveBatch(conn, aBeans);
		}
		catch (Exception e)
		{
			throw e;
		}
		finally
		{
			if (conn != null)
			{
				conn.close();
			}
		}
	}

	public static ${beanClass!}[] getBeansFromQueryBO(String soureBO, Map parameter) throws Exception
	{
		Connection conn = null;
		ResultSet resultset = null;
		try
		{
			conn = ServiceManager.getSession().getConnection();
			String sql = ServiceManager.getObjectTypeFactory().getInstance(soureBO).getMapingEnty();
			resultset = ServiceManager.getDataStore().retrieve(conn, sql, parameter);
			return (${beanClass!}[]) ServiceManager.getDataStore().crateDtaContainerFromResultSet(${beanClass!}.class, ${beanClass!}.getObjectTypeStatic(), resultset, null, true);
		}
		catch (Exception e)
		{
			throw e;
		}
		finally
		{
			if (resultset != null)
			{
				resultset.close();
			}
			if (conn != null)
			{
				conn.close();
			}
		}
	}

	public static ${beanClass!}[] getBeansFromSql(String sql, Map parameter) throws Exception
	{
		Connection conn = null;
		ResultSet resultset = null;
		try
		{
			conn = ServiceManager.getSession().getConnection();
			resultset = ServiceManager.getDataStore().retrieve(conn, sql, parameter);
			return (${beanClass!}[]) ServiceManager.getDataStore().crateDtaContainerFromResultSet(${beanClass!}.class, ${beanClass!}.getObjectTypeStatic(), resultset, null, true);
		}
		catch (Exception e)
		{
			throw e;
		}
		finally
		{
			if (resultset != null)
			{
				resultset.close();
			}
			if (conn != null)
			{
				conn.close();
			}
		}
	}

	public static java.math.BigDecimal getNewId() throws Exception
	{
		return ServiceManager.getIdGenerator().getNewId(${beanClass!}.getObjectTypeStatic());
	}

	public static Timestamp getSysDate() throws Exception
	{
		return ServiceManager.getIdGenerator().getSysDate(${beanClass!}.getObjectTypeStatic());
	}

	public static ${beanClass!} wrap(DataContainerInterface source, Map colMatch, boolean canModify)
	{
		try
		{
			return (${beanClass!}) DataContainerFactory.wrap(source, ${beanClass!}.class, colMatch, canModify);
		}
		catch (Exception e)
		{
			if (e.getCause() != null)
			{
				throw new RuntimeException(e.getCause());
			}
			else
			{
			throw new RuntimeException(e);
			}
		}
	}

	public static ${beanClass!} copy(DataContainerInterface source, Map colMatch, boolean canModify)
	{
		try
		{
			${beanClass!} result = new ${beanClass!}();
			DataContainerFactory.copy(source, result, colMatch);
			return result;
		}
		catch (AIException ex)
		{
			if (ex.getCause() != null)
			{
				throw new RuntimeException(ex.getCause());
			}
			else
			{
				throw new RuntimeException(ex);
			}
		}
	}

	public static ${beanClass!} transfer(${valueClass!} value)
	{
		if (value == null)
		{
			return null;
		}
		try
		{
			if (value instanceof ${beanClass!})
			{
				return (${beanClass!}) value;
			}
			${beanClass!} newBean = new ${beanClass!}();

			DataContainerFactory.transfer(value, newBean);
			return newBean;
		}
		catch (Exception ex)
		{
			if (ex.getCause() != null)
			{
				throw new RuntimeException(ex.getCause());
			}
			else
			{
				throw new RuntimeException(ex);
			}
		}
	}

	public static ${beanClass!}[] transfer(${valueClass!}[] value)
	{
		if (value == null || value.length == 0)
		{
			return null;
		}

		try
		{
			if (value instanceof ${beanClass!}[])
			{
				return (${beanClass!}[]) value;
			}
			${beanClass!}[] newBeans = new ${beanClass!}[value.length];
			for (int i = 0; i < newBeans.length; i++)
			{
				newBeans[i] = new ${beanClass!}();
				DataContainerFactory.transfer(value[i], newBeans[i]);
			}
			return newBeans;
		}
		catch (Exception ex)
		{
			if (ex.getCause() != null)
			{
				throw new RuntimeException(ex.getCause());
			}
			else
			{
			throw new RuntimeException(ex);
			}
		}
	}

	public static void save(${valueClass!} aValue) throws Exception
	{
		save(transfer(aValue));
	}

	public static void save(${valueClass!}[] aValues) throws Exception
	{
		save(transfer(aValues));
	}

	public static void saveBatch(${valueClass!}[] aValues) throws Exception
	{
		saveBatch(transfer(aValues));
	}
}
