package ${daoImplPackage!};

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import com.asiainfo.areca.exceptions.Throws;
import com.asiainfo.areca.exceptions.CommonError;

import com.ai.appframe2.bo.DataContainer;
import com.asiainfo.areca.soa.service.annotations.DBRoute;
import com.asiainfo.areca.soa.service.annotations.RouteTypeEnum;
import com.asiainfo.areca.express.rse.RSExpressDAO;
import ${beanPackage!}.${beanClass!};
import ${enginePakage!}.${engineClass!};
import ${valuePackage!}.${valueClass!};
import ${daoPackage!}.${daoClass!};

import java.util.*;

public class ${daoImplClass!} extends RSExpressDAO implements ${daoClass!}
{

    @Override
    public int saveInstance(${beanClass!} dc) throws Exception{
        if (null == bean) {
            Throws.error(CommonError.NULL_ERROR);
        }

        return  ${engineClass!}.save2(bean);
    }

    @Override
    public int batchSaveInstance(${beanClass!}[] dcs) throws Exception{
        if(ArrayUtils.isEmpty(dcs)){
            Throws.error(CommonError.NULL_ERROR);
        }
        return  ${engineClass!}.saveBatch2(dcs);
    }

<#list columnInfos as columnInfo>
<#if columnInfo.keyIs>
    @Override
<#if routeFlag>
    @DBRoute(routeType = RouteTypeEnum.RegionId, parameterIndex = 1)
</#if>
    public DataContainer getByPk${columnInfo.firstUpperName}(${columnInfo.boDataType} ${columnInfo.firstLowerName} <#if routeFlag>,String regionId</#if>) throws Exception{
        Map<String, String> param = new HashMap<>();
        param.put("${columnInfo.columnName}", ${columnInfo.firstLowerName});
        return this.executeQuery("SEL_${columnInfo.tableName}_BY_${columnInfo.columnName}", param);
    }
</#if>

<#if columnInfo.idxIs && !columnInfo.keyIs>
    @Override
<#if routeFlag>
    @DBRoute(routeType = RouteTypeEnum.RegionId, parameterIndex = 1)
</#if>
    public DataContainer[] listBy${columnInfo.firstUpperName}(${columnInfo.boDataType} ${columnInfo.firstLowerName}<#if routeFlag>,String regionId</#if>) throws Exception{
        Map<String, String> param = new HashMap<>();
        param.put("${columnInfo.columnName}", ${columnInfo.firstLowerName});
        return this.executeQuery("SEL_${columnInfo.tableName}_BY_${columnInfo.columnName}", param);
    }
</#if>
</#list>
}